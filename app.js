const util = require("./utils/util")
const network = require("./utils/network")
const api = require('./utils/api')

App({
    onLaunch() {
        network.initApp(this);
        this.initGlobalData()
    },
    // 初始化全局变量
    initGlobalData() {
        wx.setStorageSync('oneManSleep', null)
        wx.setStorageSync('smartRelax', null)
        this.globalData.token = wx.getStorageSync('token')
        this.globalData.user_id = wx.getStorageSync('user_id')
    },

    //监听全局变量
    watchProperty: function (method, attrs, value) {
        let obj = this.globalData;
        Object.defineProperty(obj, attrs, {
            configurable: true,
            enumerable: true,
            set: function (_value) {
                value = _value;
                method(value);
            },
            get: function () {
                return value;
            }
        })
    },
    globalData: {
        websize: 'http://192.168.11.97:8081/',
        openid: null,
        sessionKey: null,
        loginFlagIndex: false,
        loginFlagProfile: false,
        userId: null,
        deviceId: null,
        deviceId2: null,
        failDevice: null, //重连失败备用设备号
        failDevice2: null,
        scan_device_id: null,
        token: null,
        nopermission: true,
        bluetoothInfo: null,
        bluetoothInfo2: null,
        needsubscribe: null,
        subscribeFlag: true,
        needConnect: false,
        playmusic: null,
        storeId: null,
        bluetooth_connected: false,
        bluetooth_connected2: false,
        bedside: 0,
        bedInfo: [],
        stateInfo: [],
        hasChecked: null,
        dataswitch: null,
        hasalarm: false,
        BRDATA: [],
        HRDATA: [],
        setBR: [],
        setHR: [],
    }
})