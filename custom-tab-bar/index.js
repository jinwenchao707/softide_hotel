Component({
  data: {
    show: true, //是否显示导航
    currentTab: 0, // 默认首页为选中页面
    color: "#7A7E83",
    selectedColor: "#1EC6FF",
    items: [{
        pagePath: "/pages/index/index",
        iconPath: "/images/custom/index.png",
        selectedIconPath: "/images/custom/index_active.png",
        text: "首页"
      },
      {
        pagePath: "/pages/report/report",
        iconPath: "/images/custom/report.png",
        selectedIconPath: "/images/custom/report_active.png",
        text: "报告"
      },
      {
        "pagePath": "/pages/shop/shop",
        "iconPath": "/images/custom/shop.png",
        "selectedIconPath": "images/custom/shop_active.png",
        "text": "商城"
      },
      {
        pagePath: "/pages/profile/profile",
        iconPath: "/images/custom/profile.png",
        selectedIconPath: "/images/custom/profile_active.png",
        text: "我的"
      }
    ]
  },

  methods: {
    switchTab: function (e) {
      const url = e.currentTarget.dataset.url;
      if (url == '/pages/shop/shop') {
        wx.navigateToMiniProgram({
          appId: 'wxc47f191efd430461',
          path: 'pages/index/index',
          extraData: {
            foo: 'bar'
          },
          envVersion: 'release',
          success(res) {
            // 打开成功
            console.log('跳转成功')
          }
        })

      } else {
        wx.switchTab({
          url
        })
      }

    }
  },
})