"use strict";
exports.__esModule = true;
exports.getRecommendProducts = exports.mixReportImage = exports.getQuestionDetail = exports.getSurveyList = exports.removeReport = exports.saveQuestions = void 0;
var http_1 = require("../http");
var saveQuestions = function (q) {
	return http_1["default"].post('/xcx/questionnaire/save', q);
};
exports.saveQuestions = saveQuestions;
var removeReport = function (q) {
	return http_1["default"].get('/xcx/questionnaire/delete', q);
};
exports.removeReport = removeReport;
var getSurveyList = function (q) {
	return http_1["default"].get('/xcx/questionnaire/findList', q);
};
exports.getSurveyList = getSurveyList;
var getQuestionDetail = function (q) {
	return http_1["default"].get('/xcx/questionnaire/detail', q);
};
exports.getQuestionDetail = getQuestionDetail;
var mixReportImage = function (q) {
	return http_1["default"].get('/xcx/questionnaire/mixReportImage', q);
};
exports.mixReportImage = mixReportImage;
var getRecommendProducts = function (q) {
	return http_1["default"].get('/xcx/questionnaire/findProductList', q);
};
exports.getRecommendProducts = getRecommendProducts;