let _app = null;
const util = require("./util.js")

function initApp(app) {
	_app = app;
}
// const baseUrl = 'http://192.168.9.192:8080/kesHotel'
// const baseUrl = 'http://192.168.1.163:8098/kesHotel'
const baseUrl = 'https://www.smartbed.ink/kesHotel'

function request(config) {
	console.log("config:", config);
	return new Promise((resolve, reject) => {
		wx.request({
			url: baseUrl + config.url,
			header: {
				Authorization: _app.globalData.token
			},
			method: config.method,
			data: config.data,
			success: function (res) {
				console.log("success", config.url, res)
				resolve(res);
			},
			fail: function (res) {
				console.log("fail ", config.url, res)
				reject(500);
			}
		})
	})
	// .then(res => {
	//     if (res.statusCode == 200) {
	//         if (res.data.code == 200) {
	//             let data = res.data.data;
	//             console.log('data111', data)
	//             return Promise.resolve(data == undefined || !data ? "success" : data);
	//         } else if (res.data.code == 601) { //用户不存在
	//             return Promise.resolve(601);
	//         } else {
	//             return Promise.reject(res.data.code);
	//         }
	//     } else {
	//         return Promise.reject(5000);
	//     }
	// }).catch(res => {
	//     let errMsg = "error"
	//     util.myShowToast("服务器有点小问题");
	// })
}


//床控指令
function bedControl(bedData, flag) {
	if (bedData.cabin_id == undefined) {
		toPurchase(flag);
		return;
	} else if (!_app.globalData.payFlag) {
		wx.navigateTo({
			url: '/pages/purchase/purchase?flag=' + flag,
		})
		return;
	} else {
		let bedConfig = {
			url: _app.globalData.websize + "/api/v1/cabin/controlBed",
			method: "Post",
			data: bedData
		}
		return request(bedConfig);
	}
}


module.exports = {
	initApp,
	request
}