"use strict";
exports.__esModule = true;
var index_1 = require("../utils/config/index");
var defaultBaseURL = index_1["default"].baseUrl;
exports["default"] = {
	upload: function (url, data) {
		return new Promise(function (resolve, reject) {
			wx.uploadFile({
				url: defaultBaseURL + url,
				filePath: data.filePath,
				name: "file",
				header: {
					"content-type": "application/x-www-form-urlencoded"
				},
				success: function (res) {
					var data = typeof res.data == "string" ? JSON.parse(res.data) : res.data;
					resolve(data);
				},
				fail: function (err) {
					reject(err);
				}
			});
		});
	},
	get: function (url, data, configObj) {
		return new Promise(function (resolve, reject) {
			wx.request({
				url: configObj && configObj.baseURL ?
					configObj.baseURL + url :
					defaultBaseURL + url,
				data: data,
				method: "GET",
				success: function (res) {
					var data = res.data;
					resolve(data);
				},
				fail: function (err) {
					reject(err);
				}
			});
		});
	},
	post: function (url, data, configObj) {
		return new Promise(function (resolve, reject) {
			wx.request({
				url: configObj && configObj.baseURL ?
					configObj.baseURL + url :
					defaultBaseURL + url,
				data: data,
				method: "POST",
				success: function (res) {
					var data = res.data;
					resolve(data);
				},
				fail: function (err) {
					reject(err);
				}
			});
		});
	}
};