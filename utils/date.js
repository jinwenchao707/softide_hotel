"use strict";
exports.__esModule = true;
exports.getLeaveTime = exports.formatTime = exports.formatDate = void 0;
var formatNumber = function (n) {
	var s = n.toString();
	return s[1] ? s : "0" + s;
};
var formatDate = function (val) {
	if (!val) {
		return;
	}
	var date = new Date(val);
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	return [year, month, day].map(formatNumber).join("-");
};
exports.formatDate = formatDate;
var formatTime = function (date) {
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	return [year, month, day].map(formatNumber).join("-") + ' ' + [hours, minutes, seconds].map(formatNumber).join(':');
};
exports.formatTime = formatTime;
var getLeaveTime = function (val) {
	if (!val) {
		return;
	}
	// 兼容ios
	val = val.replace(/\-/g, "/");
	var oldTimes = new Date(val).getTime();
	var times = new Date().getTime();
	var leaveTime = -oldTimes + times;
	leaveTime = leaveTime / 1000;
	var leaves = 600 - parseInt(leaveTime);
	console.log("leaveTime ====  ", leaves);
	if (leaves <= 0) {
		return {
			mins: 0,
			seconds: 0
		};
	}
	var mins = leaves % 60;
	var seconds = leaves / 60;
	console.log("Logs ====  ", {
		mins: mins,
		seconds: parseInt(seconds)
	});
	return {
		mins: mins,
		seconds: parseInt(seconds)
	};
};
exports.getLeaveTime = getLeaveTime;