const {
    request
} = require("./network.js")

/**
 * 获取用户openid
 * @param {*} data 
 */
export function getOpenId(data) {
    return request({
        url: '/api/user/getOpenIdByJsCode',
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        data: data
    })
}

/**
 * 校验用户是否存在
 * @param  data 
 * @returns
 */
export function checkOpenId(data) {
    return request({
        url: '/api/user/checkOpenId',
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        data: data
    })
}

/**
 * 用户登录
 * @param data 
 * @returns
 */
export async function login(data) {
    return await request({
        url: '/api/user/login',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        data
    })
}

/**
 * 注销账号
 * @param {*} auth 
 * @returns
 */
export async function cancellation(auth) {
    return await request({
        url: '/api/user/cancellation',
        method: 'delete',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': auth
        }
    })
}

/**
 * 查询床边信息
 * @param {String} qrcode -水牌号
 * @param {String} auth -token
 * @returns
 */
export async function bedSide(data, auth) {
    return await request({
        url: '/api/select/getSelectInfo',
        method: 'get',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 
 * @param {String} device_id -设备号
 * @param {*} auth -token
 */
export async function getBedInfoByDeviceId() {
    return await request({
        url: '/api/select/getBedInfoByDeviceId',
        method: 'get',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 绑定智能床
 * @param {String} qrcode -水牌号
 * @param {String} bed_side -床边
 * @returns
 */
export async function bind(data, auth) {
    return await request({
        url: '/api/select/bind',
        method: 'post',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 解绑智能床
 * @param data
 * @returns
 */
export async function unBind(data, auth) {
    return await request({
        url: '/api/select/unbind',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 获取用户绑定情况
 * @param {String} user_id -用户id
 * @param {String} auth -token
 */
export async function getBindInfo(data, auth) {
    return await request({
        url: '/api/select/getBindInfo',
        method: 'get',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 数据开关设置
 * @param data
 * @returns
 */
export async function setDataSwitch(data, auth) {
    return await request({
        url: '/api/qrcodeDevice/setDataSwitch',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 打鼾干预设置
 * @param data
 * @returns
 */
export async function setSnoreSwitch(data, auth) {
    return await request({
        url: '/api/qrcodeDevice/setSnoreSwitch',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 查询报告
 * @param data
 * @returns
 */
export function sleepReport(data, auth) {
    return request({
        url: '/api/sleep/healthAndSleepReport',
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 获取已有报告日期
 * @param data
 * @returns
 */
export function getReportDateList(data, auth) {
    return request({
        url: '/api/sleep/getReportDateList',
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 查询压力报告
 * @param data
 * @param auth
 * @returns
 */
export function oneSleepReport(data, auth) {
    return request({
        url: '/api/sleep/oneSleepReport',
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data: data
    })
}

/**
 * 场景控制
 * @param data
 * @param auth
 * @returns
 */
export async function setBedControl(data, auth) {
    return await request({
        url: '/api/qrcodeDevice/setBedControl',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 按键记录
 * @param data
 * @param auth
 * @returns
 */
export async function insertRemoteKey(data, auth) {
    return await request({
        url: '/api/record/insertRemoteKey',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * @param data
 * @returns
 */
export function getDeviceIdListByCards(data) {
    return request({
        url: '/api/qrcodeDevice/getQrcodeDeviceList',
        method: 'get',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        data: data
    })
}

/**
 * 满意度问卷
 * @param data
 * @returns
 */
export async function insertQuestionnaire(data, auth) {
    return await request({
        url: '/api/record/insertQuestionnaire',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 获取闹钟
 * @param {String} auth 
 * @returns {Promise<>}
 */
export async function getAlarmClockList(auth) {
    return await request({
        url: '/api/alarmClock/getAlarmClockList',
        method: 'get',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        }
    })
}

/**
 * 添加闹钟
 * @param {String} cron -时间格式YYYY-MM-DD hh:mm:ss
 * @returns{Promise<>}
 */
export async function addAlarmClock(data, auth) {
    return await request({
        url: '/api/alarmClock/addAlarmClock',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 更新闹钟
 * @param {number} id -用户id
 * @param {String} cron -闹钟时间，格式同上
 * @returns 
 */
export async function updateAlarmClock(data, auth) {
    return await request({
        url: '/api/alarmClock/updateAlarmClock',
        method: 'post',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}

/**
 * 删除闹钟
 * @param {number} id -闹钟id
 * @returns
 */
export async function deleteAlarmClock(data, auth) {
    return await request({
        url: '/api/alarmClock/deleteAlarmClock',
        method: 'DELETE',
        baseURL: '/kesHotel',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': auth
        },
        data
    })
}