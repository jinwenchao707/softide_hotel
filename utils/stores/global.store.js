"use strict";
exports.__esModule = true;
var global_store_1 = require("mobx-miniprogram");
exports["default"] = (0, global_store_1.observable)({
	userInfo: {
		openid: '',
		member_id: '',
		nickname: '',
		headimgurl: '',
		phone: ""
	},
	get isLogin() {
		return !!this.userInfo.nickname;
	},
	updateUserInfo: function (val) {
		var _this = this;
		var target = {
			openid: '',
			member_id: '',
			nickname: '',
			headimgurl: '',
			phone: ""
		};
		Object.keys(this.userInfo).forEach(function (key) {
			target[key] = val[key] || _this.userInfo[key];
		});
		this.userInfo = target;
	},
	updateUserKey: function (key, val) {
		this.userInfo[key] = val;
	},
	authLogin: function (callback) {
		if (this.isLogin) {
			callback && callback();
		} else {
			wx.navigateTo({
				url: '/pages/login/index'
			});
		}
	}
});