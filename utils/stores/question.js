"use strict";
exports.__esModule = true;
var questionList = [{
		startup: 'basic'
	},
	{
		catagroy: '基础信息',
		moduleTotal: 5,
		list: [{
				id: 'Q1',
				title: '年龄',
				icon: 'tree',
				value: '',
				type: 'radio',
				moduleIndex: 0,
				options: [{
						name: '24以下',
						value: '24以下'
					},
					{
						name: '25-35',
						value: '25-35'
					},
					{
						name: '36-45',
						value: '36-45'
					},
					{
						name: '46-55',
						value: '46-55'
					},
					{
						name: '55以上',
						value: '55以上'
					},
				]
			},
			{
				moduleIndex: 1,
				id: 'Q2',
				title: '性别',
				icon: 'gender',
				value: '',
				type: 'sex'
			},
			{
				moduleIndex: 1,
				id: 'Q2a',
				title: '当前处于哪个阶段？',
				icon: 'woman',
				value: '',
				type: 'radio',
				relationParentValue: '1',
				relationParent: 'Q2',
				options: [{
						name: '备孕期',
						value: 'A'
					},
					{
						name: '怀孕期',
						value: 'B'
					},
					{
						name: '都不是',
						value: 'C'
					},
				]
			},
			{
				moduleIndex: 2,
				id: 'Q3',
				title: '身高体重',
				icon: 'ruler',
				type: 'multi-input',
				sublist: [{
						title: '身高',
						value: '',
						unit: 'cm'
					},
					{
						title: '体重',
						value: '',
						unit: 'kg'
					},
				]
			},
			{
				moduleIndex: 3,
				id: 'Q4',
				title: '睡姿',
				icon: 'sleep-bed',
				value: '',
				type: 'radio',
				options: [{
						name: '仰卧',
						value: '仰卧'
					},
					{
						name: '侧卧',
						value: '侧卧'
					},
					{
						name: '俯卧',
						value: '俯卧'
					},
				]
			},
			{
				moduleIndex: 4,
				relationParentValue: '2',
				relationParent: 'Q2',
				id: 'Q5a',
				title: '穿衣指数',
				icon: 'style',
				value: '',
				type: 'radio',
				options: [{
						name: '160-XS',
						value: '165-XS'
					},
					{
						name: '165-S',
						value: '165-S'
					},
					{
						name: '170-M',
						value: '170-M'
					},
					{
						name: '175-L',
						value: '175-L'
					},
					{
						name: '180-XL',
						value: '180-XL'
					},
					{
						name: '185-XXL',
						value: '185-XXL'
					},
					{
						name: '190-XXXL',
						value: '190-XXXL'
					},
				]
			},
			{
				moduleIndex: 4,
				relationParentValue: '1',
				relationParent: 'Q2',
				id: 'Q5b',
				title: '穿衣指数',
				icon: 'style',
				value: '',
				type: 'radio',
				options: [{
						name: '150-XS',
						value: '150-XS'
					},
					{
						name: '155-S',
						value: '155-S'
					},
					{
						name: '160-M',
						value: '160-M'
					},
					{
						name: '165-L',
						value: '165-L'
					},
					{
						name: '170-XL',
						value: '170-XL'
					},
					{
						name: '172-XXL',
						value: '172-XXL'
					},
					{
						name: '175-XXXL',
						value: '175-XXXL'
					},
				]
			},
		]
	},
	{
		startup: 'sleep'
	},
	{
		catagroy: '睡眠情况',
		moduleTotal: 9,
		list: [{
				id: 'Q6',
				title: '近一个月，您晚上上床睡觉的时间通常是几点？',
				icon: 'clock',
				value: '22:00',
				type: 'time-clock'
			},
			{
				id: 'Q7',
				title: '近一个月，从上床到入睡通常需要多少分钟？',
				icon: 'bed',
				value: '22:00',
				type: 'radio',
				options: [{
						name: '小于15分钟',
						value: 'A'
					},
					{
						name: '16-30分钟',
						value: 'B'
					},
					{
						name: '31-60分钟',
						value: 'C'
					},
					{
						name: '大于60分钟',
						value: 'D'
					},
				]
			},
			{
				id: 'Q8',
				title: '近一个月，通常早上通常几点钟起床？',
				icon: 'clock',
				value: '07:00',
				type: 'time-clock'
			},
			{
				id: 'Q9',
				title: '近一个月，每夜实际睡眠几小时？',
				icon: 'clock',
				value: '7.5',
				type: 'time-count'
			},
			{
				id: 'Q10a',
				title: '近一个月，您是否因为以下问题影响睡眠而烦恼?',
				subTitle: '【 a 】入睡困难（30 分钟内不能入睡）',
				icon: 'bed',
				type: 'radio',
				value: '',
				indexOfModule: 4,
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10b',
				title: '【 b 】夜间易醒或早醒',
				icon: 'sleepb',
				value: '',
				type: 'radio',
				indexOfModule: 4,
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10c',
				title: '【c】夜间去厕所',
				icon: 'sleepc',
				indexOfModule: 4,
				value: '',
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10d',
				title: '【d】出现呼吸不畅',
				icon: 'sleepd',
				value: '',
				indexOfModule: 4,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10e',
				title: '【e】响亮的鼾声或咳嗽声',
				icon: 'bed',
				value: '',
				indexOfModule: 4,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10f',
				title: '【f】感到太冷',
				icon: 'sleepf',
				value: '',
				indexOfModule: 4,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10g',
				title: '【g】感到太热',
				icon: 'sleepg',
				value: '',
				type: 'radio',
				indexOfModule: 4,
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10h',
				title: '【h】做噩梦',
				icon: 'bed',
				value: '',
				type: 'radio',
				indexOfModule: 4,
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10i',
				title: '【i】感到疼痛',
				icon: 'sleepi',
				value: '',
				indexOfModule: 4,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q10j',
				title: '【j】其他影响睡眠的事情',
				icon: 'bed',
				value: '',
				indexOfModule: 4,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '< 1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '> 3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q11',
				title: '近一个月，总的来说，您认为自己的睡眠?',
				icon: 'bed',
				value: '',
				indexOfModule: 5,
				type: 'radio',
				options: [{
						name: '很好',
						value: 'A'
					},
					{
						name: '较好',
						value: 'B'
					},
					{
						name: '较差',
						value: 'C'
					},
					{
						name: '很差',
						value: 'D'
					},
				]
			},
			{
				id: 'Q12',
				title: '近一个月，您用药来催眠的情况?',
				icon: 'drugs',
				value: '',
				indexOfModule: 6,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '小于1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '大于3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q13',
				title: '近一个月，您常感到困倦吗?',
				icon: 'sleep-bed',
				value: '',
				indexOfModule: 7,
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '小于1 次/周',
						value: 'B'
					},
					{
						name: '1-2 次/周',
						value: 'C'
					},
					{
						name: '大于3 次/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q14',
				title: '近一个月，您有感到做事情精力不足吗?',
				icon: 'sleep-bed',
				value: '',
				indexOfModule: 8,
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
				]
			},
		]
	},
	{
		startup: 'pressure'
	},
	{
		catagroy: '心理压力',
		list: [{
				id: 'Q15',
				title: '近一个月，当意想不到的事情发生时感到烦躁？',
				icon: 'no-smile',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q16',
				title: '近一个月，感到自己无法控制生活中重要的事情？',
				icon: 'no-face',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q17',
				title: '近一个月，感到紧张不安和有压力？',
				icon: 'cry',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q18',
				title: '近一个月，自己有信心能够处理个人问题？',
				icon: 'smile',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q19',
				title: '近一个月，所有事情正在和希望的一样发展，感到顺心如意？',
				icon: 'smile',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q20',
				title: '近一个月，自己无法处理所有必须做的事情？',
				icon: 'no-face',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q21',
				title: '近一个月，自己能控制生活中的一些恼怒情绪？',
				icon: 'no-face',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q22',
				title: '近一个月，自己能安排一切？',
				icon: 'smile',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q23',
				title: '近一个月，常常生气，因为事情的发生超出自己的控制？',
				icon: 'cry',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
			{
				id: 'Q24',
				title: '近一个月，常感到困难的事情堆积如山，而自己无法克服？',
				icon: 'cry',
				value: '',
				type: 'radio',
				options: [{
						name: '没有',
						value: 'A'
					},
					{
						name: '极少',
						value: 'B'
					},
					{
						name: '有时',
						value: 'C'
					},
					{
						name: '经常',
						value: 'D'
					},
					{
						name: '总是',
						value: 'E'
					},
				]
			},
		]
	},
	{
		startup: 'habit'
	},
	{
		catagroy: '生活习惯',
		list: [{
				id: 'Q25',
				title: '近一个月内，夜宵频次是?',
				icon: 'yexiao',
				value: '',
				type: 'radio',
				options: [{
						name: '从不夜宵',
						value: 'A'
					},
					{
						name: '1-2次/周',
						value: 'B'
					},
					{
						name: '3-5次/周',
						value: 'C'
					},
					{
						name: '每天',
						value: 'D'
					},
				]
			},
			{
				id: 'Q26',
				title: '近一个月内，经常熬夜加班吗?',
				icon: 'jiaban',
				value: '',
				type: 'radio',
				options: [{
						name: '不加班',
						value: 'A'
					},
					{
						name: '偶尔',
						value: 'B'
					},
					{
						name: '经常',
						value: 'C'
					},
					{
						name: '每天',
						value: 'D'
					},
				]
			},
			{
				id: 'Q27',
				title: '以下饮食中你吃的最多的是（可多选）?',
				icon: 'food',
				value: '',
				type: 'checkbox',
				options: [{
						name: '外卖或餐厅',
						value: 'A'
					},
					{
						name: '零食或方便食品',
						value: 'B'
					},
					{
						name: '新鲜水果蔬菜',
						value: 'C'
					},
					{
						name: '家中制作的餐食',
						value: 'D'
					},
				]
			},
			{
				id: 'Q28',
				title: '以下常喝饮品（可多选）?',
				icon: 'drink',
				value: '',
				type: 'checkbox',
				options: [{
						name: '白开水',
						value: 'A'
					},
					{
						name: '咖啡',
						value: 'B'
					},
					{
						name: '茶/奶茶',
						value: 'C'
					},
					{
						name: '碳酸饮料',
						value: 'D'
					},
					{
						name: '其他含糖饮料',
						value: 'F'
					},
					{
						name: '果汁',
						value: 'G'
					},
					{
						name: '矿泉水',
						value: 'H'
					},
				]
			},
			{
				id: 'Q29',
				title: '近期平均每周饮酒频率?',
				icon: 'beer',
				value: '',
				type: 'radio',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '1-3份酒精饮料/周',
						value: 'B'
					},
					{
						name: '4-5份酒精饮料/周',
						value: 'C'
					},
					{
						name: '大于6份酒精饮料/周',
						value: 'D'
					},
				]
			},
			{
				id: 'Q30',
				title: '近半年，平均每月饮酒过量（喝高/超过自己酒量）的频率?',
				icon: 'beer',
				value: '',
				type: 'radio',
				relationParent: 'Q29',
				relationParentValue: 'A',
				options: [{
						name: '无',
						value: 'A'
					},
					{
						name: '每月1-2次',
						value: 'B'
					},
					{
						name: '每月3-6次',
						value: 'C'
					},
					{
						name: '每月大于7次',
						value: 'D'
					},
				]
			},
			{
				id: 'Q31',
				title: '近半年抽烟频率?',
				icon: 'smoke',
				value: '',
				type: 'radio',
				columns: 2,
				options: [{
						name: '从不吸烟',
						value: 'A'
					},
					{
						name: '每周1-3包',
						value: 'B'
					},
					{
						name: '每天1包',
						value: 'C'
					},
					{
						name: '每天1.5包及以上',
						value: 'D'
					},
				]
			},
			{
				id: 'Q32',
				title: '近一个月内，平均每周运动频率?',
				icon: 'fit',
				value: '',
				type: 'radio',
				options: [{
						name: '几乎没有',
						value: 'A'
					},
					{
						name: '1-2次',
						value: 'B'
					},
					{
						name: '3-5次',
						value: 'C'
					},
					{
						name: '每天',
						value: 'D'
					},
				]
			},
			{
				id: 'Q33',
				title: '近一个月内，平均每周运动强度?',
				icon: 'habit9',
				value: '',
				type: 'radio',
				relationParent: 'Q32',
				relationParentValue: 'A',
				options: [{
						name: '较为轻松',
						value: 'A'
					},
					{
						name: '中等强度',
						value: 'B'
					},
					{
						name: '高强度',
						value: 'C'
					},
				]
			},
			{
				id: 'Q34',
				title: '近一个月内，平均每周运动时间?',
				icon: 'habit10',
				value: '',
				type: 'radio',
				relationParent: 'Q32',
				relationParentValue: 'A',
				options: [{
						name: '小于1小时',
						value: 'A'
					},
					{
						name: '1-2小时',
						value: 'B'
					},
					{
						name: '3-5小时',
						value: 'C'
					},
					{
						name: '大于5小时',
						value: 'D'
					},
				]
			},
			{
				id: 'Q35',
				title: '近一个月内，运动时间安排在?',
				icon: 'habit11',
				value: '',
				type: 'radio',
				relationParent: 'Q32',
				relationParentValue: 'A',
				options: [{
						name: '4:00-12:00',
						value: 'A'
					},
					{
						name: '12:00-18:00',
						value: 'B'
					},
					{
						name: '18:00-21:00',
						value: 'C'
					},
					{
						name: '21点以后',
						value: 'D'
					},
				]
			},
			{
				id: 'Q36',
				title: '近半年内，有以下情况吗？（可多选）',
				icon: 'cold',
				value: '',
				type: 'checkbox',
				options: [{
						name: '双脚冰冷',
						value: 'A'
					},
					{
						name: '感冒比较多',
						value: 'B'
					},
					{
						name: '腰椎不适',
						value: 'C'
					},
					{
						name: '颈椎不适',
						value: 'D'
					},
					{
						name: '打鼾（自己或另一半）',
						value: 'E'
					},
					{
						name: '灯光让人难以入睡',
						value: 'F'
					},
					{
						name: '声音干扰睡眠',
						value: 'G'
					},
					{
						name: '以上都没有',
						value: 'H'
					},
				]
			},
		]
	},
];
exports["default"] = questionList;