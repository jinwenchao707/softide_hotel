// pages/profile/antiSnore/antisnore.js
const app = getApp()
const api = require('../../../utils/api')
const util = require('../../../utils/util')
const bluetoothUtils = require("../../../utils/bluetoothUtils")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    switch_value: false,
    showdemo: false,
    dialogFlag: false,
    showdemo_text1: true,
    demoTime: ''
  },
  handleSnore: function () {
    let value = this.data.switch_value
    if (this.data.switch_value == true) {
      util.myShowLoading('正在关闭')
    } else {
      util.myShowLoading('正在打开')
    }
    let data = {
      open_switch: 0
    }
    if (value == false) {
      data.open_switch = 1
    } else {
      data.open_switch = 0
    }
    api.setSnoreSwitch(data, app.globalData.token).then(res => {
      if (res.data.code == 200) {
        console.log(res)
        util.myHideLoading()
        if (value == false) {
          this.setData({
            switch_value: true
          })
        } else {
          this.setData({
            switch_value: false
          })
        }
      } else {
        util.myShowToast(res.data.msg)
      }
    })
  },
  openSwitch: function () {
    let data = {
      open_switch: 1
    }
    api.setSnoreSwitch(data, app.globalData.token).then(res => {
      if (res.data.code == 200) {
        console.log(res)
        util.myHideLoading()
        this.setData({
          showdemo_text1: true,
          switch_value: true,
          showdemo: false
        })
      } else {
        util.myShowToast(res.data.msg)
        this.setData({
          showdemo_text1: true,
          showdemo: false
        })
      }
    })
    bluetoothUtils.sendMsg('AA03000A00040100000008F2F355')
  },
  openDemo: function () {
    this.setData({
      showdemo: true
    })
    bluetoothUtils.sendMsg('AA0100040007F355')
    let timeout = setTimeout(() => {
      this.setData({
        showdemo_text1: false
      })
    }, 40000)
    this.setData({
      demoTime: timeout
    })
  },
  closeDemo: function () {
    const _this = this
    _this.setData({
      showdemo: false,
      showdemo_text1: true
    })
    clearTimeout(_this.data.demoTime)
    bluetoothUtils.sendMsg('AA03000A00040100000008F2F355')
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      switch_value: options.switch == '关闭' ? false : true
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})