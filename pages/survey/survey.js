// pages/survey/survey.js
const app = getApp()
const util = require("../../utils/util")
const api = require("../../utils/api")
Page({

    /**
     * 页面的初始数据
     */
    data: {
        dialogflag: false,
        send_end: false,
        List: [{
            q: '1.您为什么会选择酒店舒福德智能床房间？',
            a: [
                '朋友推荐', '酒店推荐', '平台推广', '随机选择'
            ],
        }, {
            q: '2.您最喜欢的舒福德智能床的功能是？',
            a: ['一键入眠', '智能放松', '遥控升降按摩', '睡眠报告', '其他']
        }, {
            q: '3.舒福德智能床对您的放松、睡眠是否有所帮助？',
            a: ['非常有帮助', '有帮助', '一般', '没有帮助']
        }, {
            q: '4.您是否愿意再次入住舒福德智能床酒店？',
            a: ['非常愿意', '愿意', '不愿意', '非常不愿意', '无所谓']
        }],
        radioList: [{
            id: 1,
            radio: -1
        }, {
            id: 2,
            radio: -1
        }, {
            id: 3,
            radio: -1
        }, {
            id: 4,
            radio: -1
        }],
        icon: {
            normal: '/images/survey/select.png',
            active: '/images/survey/select_active.png',
        },
        message: ''
    },
    onChange(event) {
        console.log('event', event)
        console.log('detail', event.currentTarget.dataset.group)
        let radioIndex = "radioList[" + event.currentTarget.dataset.group + "].radio"
        this.setData({
            [radioIndex]: event.detail,
        });
    },
    gettime() {
        let time = new Date()
        let year = time.getFullYear()
        let month = time.getMonth() + 1
        let day = time.getDate()
        month = month < 10 ? '0' + month : month
        day = day < 10 ? '0' + day : day
        return year + '-' + month + '-' + day
    },
    submit: function () {
        const _this = this
        console.log('submit', this.data.radioList)
        let list = this.data.radioList
        let temp = 0
        list.forEach(item => {
            if (item.radio == -1) {
                temp = temp + 1
            }
        })
        if (temp >= 1) {
            wx.showToast({
                title: '您还未填写问卷，期望您能对本次体验提出宝贵意见和建议',
                icon: 'none',
                duration: 2000
            });
        } else {
            let questionnaire_detail = {}
            list.forEach((item, index) => {
                let qindex = 'q' + index
                let aindex = 'a' + index
                questionnaire_detail[qindex] = this.data.List[index].q
                questionnaire_detail[aindex] = this.data.List[index].a[item.radio]
            })
            questionnaire_detail.qe = '5.您的宝贵建议'
            questionnaire_detail.ae = this.data.message
            console.log('questionnaire_detail', questionnaire_detail)
            let data = {
                user_id: app.globalData.userId,
                date: this.gettime(),
                questionnaire_detail: questionnaire_detail
            }
            api.insertQuestionnaire(data, app.globalData.token).then(res => {
                if (res.data.code == 200) {
                    console.log('res', res.data)
                    _this.setData({
                        dialogflag: true
                    })
                    setTimeout(() => {
                        _this.setData({
                            send_end: true,
                            dialogflag: false

                        })
                    }, 1500)
                    let nowTime = new Date().getTime()
                    let tempTime = new Date().getFullYear() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getDate()
                    let questionTime = new Date(tempTime + ' 12:00:00').getTime() //当日12点毫秒数
                    let questionTime2 = standardTime + 24 * 60 * 60 * 1000 //次日12点毫秒数
                    if (nowTime < questionTime) {
                        wx.setStorageSync('questionTime', questionTime)
                    } else {
                        wx.setStorageSync('questionTime', questionTime2)
                    }
                } else {
                    console.log('error', res.data.msg)
                }
            })
        }

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        let nowTime = new Date().getTime() //获取当前毫秒数
        let questionTime = wx.getStorageSync('questionTime') == '' ? 0 : parseInt(wx.getStorageSync('subcribe')) //订阅期限毫秒数
        console.log('questionTime====>', questionTime, wx.getStorageSync('questionTime'))
        if (questionTime < nowTime) {
            this.setData({
                send_end: true
            })
        } else {
            this.setData({
                send_end: false
            })
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})