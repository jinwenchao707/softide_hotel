// pages/component/dialog1.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    targetImage: String,
    targetTitle: String,
    targetContent: String,
    targetBtn:String,
    index: String

  },

  /**
   * 组件的初始数据
   */
  data: {
    showMask: true,
    showModal: true,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    showPopup() {
      this.setData({
        showMask: true,
        showModal: true
      })
    },
    closeBtn() {
      this.setData({
        showMask: false,
        showModal: false
      })
      this.triggerEvent("toParentClose");
    },
    confirmBtn() {
      let indexTemp = this.properties.index;
      this.triggerEvent("toParent", { index: indexTemp  })
    },
  }
})
