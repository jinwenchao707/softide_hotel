// pages/component/dialog1.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    showMask: true,
    showModal: true,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    cancelBtn() {
      this.triggerEvent("toParentCancel");
    },
    allowBtn() {
      this.triggerEvent("toParentAllow")
    },
  }
})
