// pages/component/leveltab.js
Component({
	/**
	 * 组件的属性列表
	 */
	options: {
		addGlobalClass: true
	},
	properties: {
		value: {
			type: String,
			value: ''
		},
		Content1: {
			type: String
		},
		Content2: {
			type: String
		},
		Content3: {
			type: String
		},
		Content4: {
			type: String
		},
		Content5: {
			type: String
		},
		level: {
			type: String,
			value: '',
			observer: 'updata'
		},
		lowtohigh: {
			type: String,
			value: 0,
			observer: 'ordercheck'
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		levelvalue: 1,
		order: 1
	},
	lifetimes: {
		attached: function () {
			// 在组件实例进入页面节点树时执行
		},
		detached: function () {
			// 在组件实例被从页面节点树移除时执行
		},
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		updata(newval) {
			this.setData({
				levelvalue: newval
			})
		},
		ordercheck(newval) {
			this.setData({
				order: newval
			})
		}
	},
	/**
	 * 生命周期函数--监听组件页面加载
	 */

})