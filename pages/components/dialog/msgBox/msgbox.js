// pages/components/dialog/msgBox/msgbox.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        content: {
            type: String,
            default: ''
        },
        title: {
            type: String,
            default: ''
        },
        btntext: {
            type: String,
            default: ''
        },
        btntext_0: {
            type: String,
            default: ''
        },
        tips: {
            type: String,
            default: 1
        }
    },

    /**
     * 组件的初始数据
     */
    data: {},

    /**
     * 组件的方法列表
     */
    methods: {
        cancel: function (e) {
            this.triggerEvent('dialogHandle2', {
                params: false,
                text: 'cancel'
            })
        },
        confirm: function (e) {
            this.triggerEvent('dialogHandle2', {
                params: false,
                text: 'confirm'
            })
        }
    }
})