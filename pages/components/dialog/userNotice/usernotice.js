// pages/components/dialog/userNotice/usernotice.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        noticeChecked: false,
        ageChecked: false
    },

    /**
     * 组件的方法列表
     */
    methods: {
        toUserNotice: function () {
            console.log('toUserNotice')
            wx.navigateTo({
                url: '/pages/agreement/userProtocol/userprotocol',
            })
        },
        toSercet: function () {
            console.log('toSercet')
            wx.navigateTo({
                url: '/pages/agreement/privacy/privacy',
            })
        },
        //用户协议隐私协议勾选
        noticeCheck: function () {
            this.setData({
                noticeChecked: !this.data.noticeChecked
            })
        },
        //年满14周岁勾选
        ageCheck: function () {
            this.setData({
                ageChecked: !this.data.ageChecked
            })
        },
        onChange2: function (event) {
            console.log('change2')
            this.setData({
                ageChecked: event.detail
            })
        },
        cancel: function (e) {
            this.triggerEvent('dialogHandle', {
                params: false,
                text: 'cancel'
            })
        },
        confirm: function (e) {
            if (this.data.ageChecked == true) {
                this.triggerEvent('dialogHandle', {
                    params: false,
                    text: 'confirm'
                })
            }

        }
    }
})