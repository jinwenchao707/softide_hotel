// pages/report/report.js
const app = getApp()
const util = require("../../utils/util")
const api = require("../../utils/api")
const tipsData = require('./tips')
import * as echarts from '../../ec-canvas/echarts';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hasReport: false, //是否有报告
        hasSwitchOn: true, //是否打开数据开关
        reportDate: '',
        sleepdata: {},
        ec: {
            lazyLoad: true,
        },
        statistics: {},
        product: {
            msg: '瑜伽+按摩三段式放松身心，减轻身体酸痛感，舒展全身智能二重奏灵活选配。'
        },
        datechooseFlag: false,
        dialogShow: false,
        dateList: [],
        colorList: {
            color1: '',
            color2: '',
            color3: '',
            color4: '',
            color5: '',
            color6: '',
            color7: '',
            color8: '',
            color9: '',
            color10: '',
            color11: '',
        },
        msg: {
            content: '',
            title: '',
            btntext: '',
            tips: false
        },
        scoreImg: '',
        stageImg: '',
        snoreImg: ''
    },
    showtips: function (e) {
        let temp = e.currentTarget.dataset.index
        if (temp == 0) {
            this.setData({
                dialogShow: true,
                ['msg.content']: '疲劳指数表示睡前的身体状态，分为1-5五个等级，数值越高表示睡前身体越疲劳。',
                ['msg.tips']: 3,
                ['msg.btntext']: '确定'
            })
        } else if (temp == 1) {
            this.setData({
                dialogShow: true,
                ['msg.content']: '恢复指数表示睡后的身体状态，分为1-5五个等级，指数越高表示恢复的越好。',
                ['msg.tips']: 3,
                ['msg.btntext']: '确定'
            })
        }

    },
    selectDate: function () {
        this.setData({
            datechooseFlag: true
        })
        // console.error('datechooseFlag============>', this.data.datechooseFlag)
    },
    selectDatedraw: function (e) {
        util.myShowLoading('加载中')
        const _this = this
        console.log('eeee', e.currentTarget.dataset.index, this.data.dateList)
        let index = e.currentTarget.dataset.index
        this.setData({
            scoreImg: '',
            stageImg: '',
            snoreImg: '',
            reportDate: this.data.dateList[index]
        })
        this.getReport()
    },
    closeDate: function () {
        this.setData({
            datechooseFlag: false
        })
        this.sleepDataHandle(this.data.sleepdata)
    },
    getReport: function () {
        const _this = this
        let data = {
            date: this.data.reportDate
        }
        api.sleepReport(data, app.globalData.token).then(res => {
            if (res.data.code == 200) {
                console.log('sleep_grade', res.data.data.sleep_grade)
                if (res.data.data.sleep_grade != null) {
                    res.data.data.sleep_efficiency = (res.data.data.sleep_efficiency * 100).toFixed(1)
                    res.data.data.compare_sleep_efficiency = (res.data.data.compare_sleep_efficiency * 100).toFixed(1) + '%'
                    _this.setData({
                        hasReport: true,
                        sleepdata: res.data.data,
                        datechooseFlag: false
                    })
                    _this.sleepDataHandle(_this.data.sleepdata)
                    _this.statisticsHandle(_this.data.sleepdata)
                    console.log('reportData', res.data.data)
                } else {
                    _this.setData({
                        hasReport: false
                    })
                }
                util.myHideLoading()
            } else {
                util.myShowToast(res.data.msg)
                console.log('error', res.data.msg)
            }
        })
    },
    statisticsHandle: function (data) {
        //睡眠tips
        const _this = this
        let sleeptext = data.health_like_apnea_mark
        console.log('sleep', sleeptext)
        if (sleeptext == '正常') {
            this.setData({
                'statistics.sleep': tipsData.sleep.normal
            })
        } else if (sleeptext == '轻度') {
            this.setData({
                'statistics.sleep': tipsData.sleep.low
            })
        } else if (sleeptext == '中度') {
            this.setData({
                'statistics.sleep': tipsData.sleep.medium
            })
        } else {
            this.setData({
                'statistics.sleep': tipsData.sleep.high
            })
        }
        //心率
        let heartslow = data.health_heart_rate_slower
        let heartfast = data.health_heart_rate_faster
        if (heartslow == 0 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.normal
            })
        } else if (heartslow == 0 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1.replace('*', (heartfast + ''))
            })
        } else if (heartslow == 0 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2.replace('*', (heartfast + ''))
            })
        } else if (heartslow <= 30 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.slow1.replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.slow2.replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2_slow2.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1_slow2.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow <= 30 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1_slow1.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow <= 30 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2_slow1.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        }
        //呼吸
        let breathlow = data.health_breath_rate_slower
        let breathfast = data.health_breath_rate_faster
        if (breathlow == 0 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.normal
            })
        } else if (breathlow == 0 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1.replace('*', (breathfast + ''))
            })
        } else if (breathlow == 0 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2.replace('*', (breathfast + ''))
            })
        } else if (breathlow <= 30 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.slow1.replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.slow2.replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2_slow2.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1_slow2.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow <= 30 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1_slow1.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow <= 30 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2_slow1.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        }
    },
    sleepDataHandle: function (data) {
        //睡眠时长
        this.setData({
            stageImg: false,
            snoreImg: false,
            scoreImg: false,
        })
        let sleepduration = data.sleep_duration
        let hour = Math.floor(sleepduration / 60)
        let min = sleepduration % 60
        console.log('sleeptime========>', data.sleep_time)
        this.setData({
            'sleepdata.hour': hour,
            'sleepdata.min': min,
            'sleepdata.sleep_grade': data.sleep_grade,
            'sleepdata.my_sleep_time': data.sleep_time.substring(10, 16),
            'sleepdata.my_wake_time': data.sleep_get_up_time.substring(10, 16),
            'sleepdata.sleep_efficiency': data.sleep_efficiency,
            'sleepdata.compare_sleep_efficiency': data.compare_sleep_efficiency,
            'sleepdata.deep': Math.floor(data.sleep_duration_deep / 60) + '小时' + data.sleep_duration_deep % 60 + "分",
            'sleepdata.compare_deep_sleep_duration': data.compare_deep_sleep_duration,
            'sleepdata.sleep_fatigue_degree': data.sleep_fatigue_degree,
            'sleepdata.compare_fatigue_degree': data.compare_fatigue_degree,
            'sleepdata.sleep_recover_degree': data.sleep_recover_degree,
            'sleepdata.compare_recover_degree': data.compare_recover_degree,
            'sleepdata.sleep_snore_times': data.sleep_snore_times,
            'sleepdata.sleep_anti_snore_times': data.sleep_anti_snore_times,
            'sleepdata.describe': data.describe,
            'product.picUrl': data.img_url,
            'product.name': data.title,
            'product.func': data.function,
            'product.msg': data.function_describe
        })
        this.datalevel(data)
        //睡眠得分绘制
        let sleepScoreChart = this.selectComponent('#mychart-sleepscore')
        console.log('sleepScoreChart', sleepScoreChart)
        this.drawSleepScore(sleepScoreChart, data.sleep_grade)
        //睡眠分期绘制
        let sleepstage = this.sleepStage(data)
        let sleepStageChart = this.selectComponent('#mychart-sleepstage')
        this.drawSleepStage(sleepStageChart, sleepstage.date, sleepstage.value)
        //打鼾干预绘制
        let antiSnoreChart = this.selectComponent('#mychart-antisnore')
        let snoredata = this.snoreTime(data.sleep_time, data.sleep_get_up_time, data.sleep_anti_snore_stage)
        this.drawAntiSnore(antiSnoreChart, snoredata)
    },
    sleepStage: function (data) {
        let stage = data.sleep_stage
        // 睡眠分期数据处理
        if (stage !== undefined && stage !== null) {
            const tempSleep = stage.split(',')
            const value = []
            const time = []
            const sleepStage = {
                date: [],
                value: []
            }
            tempSleep.forEach(element => {
                value.push(parseInt(element.split('|')[1]))
                time.push(element.split('|')[0])
            })
            for (let i = 0; i < time.length - 1; i++) {
                const interval = (new Date(time[i + 1].replace('/', /-/)).getTime() - new Date(time[i].replace('/', /-/))) / 120000
                for (let j = 0; j < interval; j++) {
                    const tempx = []
                    tempx[0] = new Date(time[i].replace('/', /-/)).getTime() + j * 120000
                    if (value[i] === 0) {
                        tempx[1] = 3
                    } else {
                        tempx[1] = value[i]
                    }
                    sleepStage.date.push(tempx[0])
                    sleepStage.value.push(tempx[1])
                }
            }
            return sleepStage
        } else {
            return {}
        }
    },
    snoreTime: function (sleep, wake, data) {
        let x = [];
        let s = [];
        data = JSON.parse(data.replaceAll("'", "\""))
        console.log('data', data)
        sleep = sleep.replace(/-/g, "/");
        wake = wake.replace(/-/g, "/");
        let start = new Date(sleep).getTime();
        let end = new Date(wake).getTime();
        let interval = (end - start) / 120000;
        for (let i = 0; i < interval; i++) {
            let x_0 = [];
            let time = i * 120000 + start;
            x_0[0] = time;
            x_0[1] = 0;
            x.push(x_0);
        }
        s = x;
        for (let j = 1; j < s.length; j++) {
            let a = s[j - 1][0];
            let a2 = s[j][0];
            for (let k = 0; k < data.length; k++) {
                let b = new Date(data[k].replace(/-/g, "/")).getTime();
                // console.log('n_x_1', b)
                if (a < b && b < a2) {
                    let x_1 = [];
                    x_1[0] = b;
                    x_1[1] = 1;
                    x.splice(j, 0, x_1);
                    // console.log('x_1', x_1)
                }
            }
        }
        return x;
    },
    datalevel: function (data) {
        let arr = []
        const _this = this
        arr.push(data.health_average_heart_rate_mark)
        arr.push(data.health_heart_rate_slower_mark)
        arr.push(data.health_heart_rate_faster_mark)
        arr.push(data.health_average_breath_rate_mark)
        arr.push(data.health_breath_slowest_mark)
        arr.push(data.health_breath_fastest_mark)
        arr.push(data.health_breath_rate_slower_mark)
        arr.push(data.health_breath_rate_faster_mark)
        arr.push(data.health_respiratory_time_ratio_mark)
        arr.push(data.health_like_apnea_mark)
        arr.push(data.health_longest_like_apnea_mark)
        let colormap = new Map()
        colormap.set('-', 'rgba(247, 247, 247, 1)')
        colormap.set('正常', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(218, 241, 214, 0.28) 46%, rgba(119, 207, 104, 0.28) 100%)')
        colormap.set('轻度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(218, 241, 214, 0.28) 46%, rgba(119, 207, 104, 0.28) 100%)')
        colormap.set('中度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 243, 211, 0.28) 46%, rgba(223, 173, 66, 0.28) 100%)')
        colormap.set('重度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('偏快', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('偏慢', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过短', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过长', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过多', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过少', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('异常', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        arr.forEach((item, index) => {
            let color = colormap.get(item)
            let colorname = 'colorList.color' + (index + 1)
            _this.setData({
                [colorname]: color
            })
        })
    },
    drawSleepScore: function (mychart, value1) {
        const that = this
        mychart.init((canvas, width, height, dpr) => {
            const chart = echarts.init(canvas, null, {
                width: width,
                height: height,
                devicePixelRatio: dpr // new
            });
            var option = {
                angleAxis: {
                    max: 100, // 满分
                    clockwise: true, // 逆时针
                    // 隐藏刻度线
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    }
                },
                radiusAxis: {
                    type: 'category',
                    // 隐藏刻度线
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    }
                },
                polar: {
                    center: ['50%', '50%'],
                    radius: '170%' //图形大小
                },
                series: [{
                    type: 'bar',
                    data: [{
                        name: '作文得分',
                        value: value1,
                        itemStyle: {
                            normal: {
                                color: { // 完成的圆环的颜色
                                    colorStops: [{
                                        offset: 0,
                                        color: 'rgba(74, 208, 155, 1)' // 0% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: 'rgba(74, 208, 155, 1)' // 100% 处的颜色
                                    }]
                                }
                            }
                        },
                    }],
                    coordinateSystem: 'polar',
                    roundCap: true,
                    barWidth: 8,
                    barGap: '-100%', // 两环重叠
                    z: 2,
                }, { // 灰色环
                    type: 'bar',
                    data: [{
                        value: 100,
                        itemStyle: {
                            color: 'rgba(245, 245, 245, 1)',
                        }
                    }],
                    coordinateSystem: 'polar',
                    roundCap: true,
                    barWidth: 8,
                    barGap: '-100%', // 两环重叠
                    radius: ['60%', '73%'],
                    z: 1
                }]
            };
            chart.setOption(option);
            setTimeout(function () {
                mychart.canvasToTempFilePath({
                    x: 0,
                    y: 0,
                    width: 350,
                    height: 200,
                    canvasId: 'mychart-sleepscore',
                    success: function (res) {
                        console.log('scoreImg:', res.tempFilePath)
                        that.setData({
                            scoreImg: res.tempFilePath // 保存生成图片路径
                        });
                    },
                    fail: function () {
                        console.log("echarts生成失败")
                    }
                }, this)
            }, 1000)
            return chart;
        })
    },
    drawSleepStage: function (mychart, xvalue, yvalue) {
        const that = this
        mychart.init((canvas, width, height, dpr) => {
            const chart = echarts.init(canvas, null, {
                width: width,
                height: height,
                devicePixelRatio: dpr // new
            });
            var option = {
                // tooltip: {
                // 	trigger: 'axis',
                // 	axisPointer: {
                // 		type: 'line',
                // 		lineStyle: {
                // 			color: '#C9D8E3',
                // 			opacity: 0.5
                // 		}
                // 	}
                // },
                grid: {
                    top: '35%',
                    right: '5%',
                    left: '7%',
                    bottom: '0%'
                },
                xAxis: {
                    type: 'category',
                    data: xvalue,
                    // splitNumber: 28060,
                    axisLabel: {
                        show: true,
                        color: '#093147'
                    },
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                },
                yAxis: [{
                    // name: 'dB',
                    type: 'value',
                    minInterval: 1,
                    axisLabel: {
                        formatter: '{value}',
                        showMinLabel: false,
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    }
                }],
                series: [{
                    data: yvalue,
                    type: 'bar',
                    barCategoryGap: '0%',
                    itemStyle: {
                        normal: {
                            color: function (params) {
                                if (params.value === 1) {
                                    return 'rgba(53, 189, 221, 1)'
                                } else if (params.value === 2) {
                                    return 'rgba(158, 221, 236, 1)'
                                } else {
                                    return 'rgba(248, 224, 210, 1)'
                                }
                            }
                        },


                    }
                }]
            };
            chart.setOption(option);
            setTimeout(function () {
                mychart.canvasToTempFilePath({
                    x: 0,
                    y: 0,
                    width: 350,
                    height: 72,
                    canvasId: 'mychart-sleepstage',
                    success: function (res) {
                        console.log('stageImg', res.tempFilePath)
                        that.setData({
                            stageImg: res.tempFilePath // 保存生成图片路径
                        });
                    },
                    fail: function () {
                        console.log("echarts生成失败")
                    }
                }, this)
            }, 1000)
            return chart;
        })
    },
    drawAntiSnore: function (mychart, data) {
        const that = this
        mychart.init((canvas, width, height, dpr) => {
            const chart = echarts.init(canvas, null, {
                width: width,
                height: height,
                devicePixelRatio: dpr // new
            });
            var option = {
                // tooltip: {
                //   trigger: "axis",
                //   formatter: function(value) {
                //     // console.log("params", value);
                //     let time = that.dateformat(value[0].data[0], 0, 1);
                //     return time.substring(11, 16) + " " + time.substring(5, 11);
                //   }
                // },
                grid: {
                    top: "10%",
                    right: "5%",
                    left: "10%",
                    bottom: "30%"
                },
                xAxis: {
                    type: "time",
                    boundaryGap: false,
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: true,
                        margin: 5,
                        color: "rgba(0,0,0,0.4)",
                        fontSize: 8,
                        fontWeight: 400,
                        formatter: function (value) {
                            // console.log('params', value);
                            var date = new Date(value);
                            // console.log('date', date);
                            var y = date.getFullYear();
                            var m = date.getMonth() + 1;
                            var d = date.getDate();
                            var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                            var min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
                            var s = date.getSeconds();
                            return h + ':' + min

                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: "rgba(151, 151, 151, 1)",
                            width: 1
                            //opacity: 0.5,
                        }
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            type: "dashed",
                            color: "#F2F6FE"
                            //opacity: 0.3,
                        }
                    }
                },
                yAxis: {
                    type: "value",
                    splitNumber: 1,
                    boundaryGap: false,
                    // interval: interval,
                    axisLabel: {
                        show: true,
                        // interval: interval,
                        formatter: "{value}",
                        showMinLabel: true,
                        color: "rgba(0, 0, 0, 0.50)",
                        fontSize: 8,
                        fontWeight: 400
                    },
                    axisLine: {
                        show: false,
                    },
                    splitLine: {
                        show: false,
                    },
                    axisTick: {
                        show: false
                    }
                },
                series: [{
                    data: data,
                    type: "bar",
                    barCategoryGap: "0%",
                    smooth: true,
                    lineStyle: {
                        color: 'rgba(0, 158, 194, 1)',
                        width: 1
                    },
                    itemStyle: {
                        color: 'rgba(0, 158, 194, 1)'
                    }
                }]
            };
            chart.setOption(option);
            setTimeout(function () {
                mychart.canvasToTempFilePath({
                    x: 0,
                    y: 0,
                    width: 350,
                    height: 72,
                    canvasId: 'mychart-antisnore',
                    success: function (res) {
                        that.setData({
                            snoreImg: res.tempFilePath // 保存生成图片路径
                        });
                    },
                    fail: function () {
                        console.log("echarts生成失败")
                    }
                }, this)
            }, 1000)
            return chart;
        })
    },
    dialogHandle2: function (e) {
        console.log('22222222')
        const _this = this
        this.setData({
            dialogShow: e.detail.params
        })
        // this.sleepDataHandle(_this.data.sleepdata)
    },
    toScreen: function () {
        let sleepdata = this.data.sleepdata
        wx.navigateTo({
            url: '/pages/report/healthScreen/healthscreen?obj=' + JSON.stringify(sleepdata),
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        if (typeof this.getTabBar === 'function' && this.getTabBar()) {
            this.getTabBar().setData({
                currentTab: 1
            })
        }
        // this.setData({
        //     stageImg: '',
        //     snoreImg: '',
        //     scoreImg: '',
        // })
        const _this = this
        let data0 = {
            user_id: app.globalData.userId
        }
        let flag = wx.getStorageSync('healthscreen')
        console.log('flag', flag)
        if (flag != true) {
            api.getBindInfo(data0, app.globalData.token).then(res => {
                if (res.data.code == 200) {
                    let hoteldata = res.data.data
                    if (hoteldata.data_switch == false) {
                        _this.setData({
                            hasSwitchOn: false
                        })
                    } else {
                        _this.setData({
                            hasSwitchOn: true
                        })
                        let datetemp = new Date()
                        let y = datetemp.getUTCFullYear()
                        let m = datetemp.getMonth() + 1
                        let d = datetemp.getDate() - 1
                        let data1 = {
                            date: y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d)
                        }
                        api.sleepReport(data1, app.globalData.token).then(res => {
                            if (res.data.code == 200) {
                                if (res.data.data.sleep_grade == 0) {
                                    if (app.globalData.hasChecked != true) {
                                        _this.setData({
                                            dialogShow: true,
                                            ['msg.content']: '由于睡眠时间较短、睡眠位置出现偏差、网络等问题，您昨晚的睡眠未能生成报告。',
                                            ['msg.tips']: 3,
                                            ['msg.btntext']: '确定'
                                        })
                                        app.globalData.hasChecked = true
                                    }
                                }
                            } else {
                                util.myShowToast(res.data.msg)
                                console.log('error', res.data.msg)
                            }
                        })
                        let data = {
                            year_month: y + '-' + (m < 10 ? '0' + m : m)
                        }
                        api.getReportDateList(data, app.globalData.token).then(res => {
                            if (res.data.code == 200) {
                                // res.data.data = res.data.data.reverse()
                                if (res.data.data.length > 0) {
                                    _this.setData({
                                        reportDate: res.data.data[0],
                                        dateList: res.data.data
                                    })
                                    _this.getReport()
                                } else {
                                    _this.setData({
                                        hasReport: false
                                    })
                                }
                            } else {
                                util.myShowToast(res.data.msg)
                                console.log('error', res.data.msg)
                            }
                        })
                    }
                } else {
                    util.myShowToast(res.data.msg)
                }
            })
        }
        wx.setStorageSync('healthscreen', undefined)
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})