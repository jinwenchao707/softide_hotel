// pages/report/healthScreen/healthscreen.js
const tipsData = require('../tips')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        sleepdata: {},
        statistics: {},
        sleep_advice: true,
    },
    datalevel: function (data) {
        let arr = []
        const _this = this
        arr.push(data.health_average_heart_rate_mark)
        arr.push(data.health_heart_rate_slower_mark)
        arr.push(data.health_heart_rate_faster_mark)
        arr.push(data.health_average_breath_rate_mark)
        arr.push(data.health_breath_slowest_mark)
        arr.push(data.health_breath_fastest_mark)
        arr.push(data.health_breath_rate_slower_mark)
        arr.push(data.health_breath_rate_faster_mark)
        arr.push(data.health_respiratory_time_ratio_mark)
        arr.push(data.health_like_apnea_mark)
        arr.push(data.health_longest_like_apnea_mark)
        let colormap = new Map()
        colormap.set('-', 'rgba(247, 247, 247, 1)')
        colormap.set('正常', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(218, 241, 214, 0.28) 46%, rgba(119, 207, 104, 0.28) 100%)')
        colormap.set('轻度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(218, 241, 214, 0.28) 46%, rgba(119, 207, 104, 0.28) 100%)')
        colormap.set('中度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 243, 211, 0.28) 46%, rgba(223, 173, 66, 0.28) 100%)')
        colormap.set('重度', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('偏快', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('偏慢', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过短', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过长', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过多', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('过少', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        colormap.set('异常', 'linear-gradient(90deg, rgba(252, 252, 252, 0.28) 0%, rgba(255, 212, 211, 0.28) 46%, rgba(223, 72, 66, 0.28) 100%)')
        arr.forEach((item, index) => {
            let color = colormap.get(item)
            let colorname = 'colorList.color' + (index + 1)
            _this.setData({
                [colorname]: color
            })
        })
    },
    statisticsHandle: function (data) {
        //睡眠tips
        const _this = this
        let sleeptext = data.health_like_apnea_mark
        console.log('sleep', sleeptext)
        if (sleeptext == '正常') {
            this.setData({
                'statistics.sleep': tipsData.sleep.normal
            })
        } else if (sleeptext == '轻度') {
            this.setData({
                'statistics.sleep': tipsData.sleep.low
            })
        } else if (sleeptext == '中度') {
            this.setData({
                'statistics.sleep': tipsData.sleep.medium
            })
        } else if (sleeptext == '-') {
            this.setData({
                sleep_advice: false
            })
        } else {
            this.setData({
                'statistics.sleep': tipsData.sleep.high
            })
        }
        //心率
        let heartslow = data.health_heart_rate_slower
        let heartfast = data.health_heart_rate_faster
        if (heartslow == 0 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.normal
            })
        } else if (heartslow == 0 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1.replace('*', (heartfast + ''))
            })
        } else if (heartslow == 0 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2.replace('*', (heartfast + ''))
            })
        } else if (heartslow <= 30 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.slow1.replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast == 0) {
            this.setData({
                'statistics.heart': tipsData.heart.slow2.replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2_slow2.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow > 30 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1_slow2.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow <= 30 && heartfast <= 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast1_slow1.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        } else if (heartslow <= 30 && heartfast > 30) {
            this.setData({
                'statistics.heart': tipsData.heart.fast2_slow1.replace('*', (heartfast + '')).replace('*', (heartslow + ''))
            })
        }
        //呼吸
        let breathlow = data.health_breath_rate_slower
        let breathfast = data.health_breath_rate_faster
        if (breathlow == 0 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.normal
            })
        } else if (breathlow == 0 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1.replace('*', (breathfast + ''))
            })
        } else if (breathlow == 0 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2.replace('*', (breathfast + ''))
            })
        } else if (breathlow <= 30 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.slow1.replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast == 0) {
            this.setData({
                'statistics.breath': tipsData.breath.slow2.replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2_slow2.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow > 30 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1_slow2.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow <= 30 && breathfast <= 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast1_slow1.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        } else if (breathlow <= 30 && breathfast > 30) {
            this.setData({
                'statistics.breath': tipsData.breath.fast2_slow1.replace('*', (breathfast + '')).replace('*', (breathlow + ''))
            })
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if (options.obj) {
            this.setData({
                sleepdata: JSON.parse(options.obj)
            })
            this.datalevel(this.data.sleepdata)
            this.statisticsHandle(this.data.sleepdata)
            console.log('sleepdata', JSON.parse(options.obj), this.data.sleepdata)
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
        wx.setStorageSync('healthscreen', true)
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        wx.setStorageSync('healthscreen', true)
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})