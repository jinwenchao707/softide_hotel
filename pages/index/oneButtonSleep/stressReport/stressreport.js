// pages/index/oneButtonSleep/stressReport/stressreport.js
const app = getApp()
const api = require('../../../../utils/api')
const util = require('../../../../utils/util')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        showtips1: false,
        showtips2: false,
        stresslevel: "",
        stressvalue: '',
        stressResistancelevel: '',
        stressResistancevalue: '',
        fatigueIndexlevel: '',
        fatigueIndexvalue: '',
        ansActivitylevel: '',
        ansActivityvalue: '',
        stressdata: {}
    },
    showtips: function (e) {
        if (e.currentTarget.dataset.card == 1) {
            console.log('test', this.data.showtips1)
            this.setData({
                showtips1: !this.data.showtips1
            })
        } else if (e.currentTarget.dataset.card == 2) {
            this.setData({
                showtips2: !this.data.showtips2
            })
        } else if (e.currentTarget.dataset.card == 3) {
            this.setData({
                showtips3: !this.data.showtips3
            })
        } else {
            this.setData({
                showtips4: !this.data.showtips4
            })
        }
    },
    //压力信息评测
    stresscheck: function (data) {
        //抗压能力
        if (data.stress_resistance >= 130 && data.stress_resistance <= 150) {
            this.setData({
                stressResistancelevel: 1,
            })
        } else if (data.stress_resistance >= 110 && data.stress_resistance < 130) {
            this.setData({
                stressResistancelevel: 2,
            })
        } else if (data.stress_resistance >= 90 && data.stress_resistance < 110) {
            this.setData({
                stressResistancelevel: 3,
            })
        } else if (data.stress_resistance >= 70 && data.stress_resistance < 90) {
            this.setData({
                stressResistancelevel: 4,
            })
        } else if (data.stress_resistance >= 0 && data.stress_resistance < 70) {
            this.setData({
                stressResistancelevel: 5,
            })
        }
        //压力指数
        if (data.stress_index >= 0 && data.stress_index <= 70) {
            this.setData({
                stresslevel: 1,
            })
        } else if (data.stress_index > 70 && data.stress_index <= 90) {
            this.setData({
                stresslevel: 2
            })
        } else if (data.stress_index > 90 && data.stress_index <= 110) {
            this.setData({
                stresslevel: 3
            })
        } else if (data.stress_index > 110 && data.stress_index <= 130) {
            this.setData({
                stresslevel: 4
            })
        } else {
            this.setData({
                stresslevel: 5
            })
        }
        //疲劳指数
        if (data.fatigue_index >= 0 && data.fatigue_index <= 70) {
            this.setData({
                fatigueIndexlevel: 1,
            })
        } else if (data.fatigue_index > 70 && data.fatigue_index <= 90) {
            this.setData({
                fatigueIndexlevel: 2
            })
        } else if (data.fatigue_index > 90 && data.fatigue_index <= 110) {
            this.setData({
                fatigueIndexlevel: 3
            })
        } else if (data.fatigue_index > 110 && data.fatigue_index <= 130) {
            this.setData({
                fatigueIndexlevel: 4
            })
        } else {
            this.setData({
                fatigueIndexlevel: 5
            })
        }
        //自主神经活性
        if (data.ans_activity >= 130 && data.ans_activity <= 150) {
            this.setData({
                ansActivitylevel: 1,
            })
        } else if (data.ans_activity >= 110 && data.ans_activity < 130) {
            this.setData({
                ansActivitylevel: 2,
            })
        } else if (data.ans_activity >= 90 && data.ans_activity < 110) {
            this.setData({
                ansActivitylevel: 3,
            })
        } else if (data.ans_activity >= 70 && data.ans_activity < 90) {
            this.setData({
                ansActivitylevel: 4,
            })
        } else if (data.ans_activity >= 0 && data.ans_activity < 70) {
            this.setData({
                ansActivitylevel: 5,
            })
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        console.log('stressdata000', JSON.parse(options.stressdata))
        this.setData({
            stressdata: JSON.parse(options.stressdata)
        })
        console.log('stressdata', this.data.stressdata)
        this.setData({
            stressvalue: this.data.stressdata.stress_index, //压力指数
            stressResistancevalue: this.data.stressdata.stress_resistance, //抗压能力
            fatigueIndexvalue: this.data.stressdata.fatigue_index, //疲劳指数
            ansActivityvalue: this.data.stressdata.ans_activity, //自主神经活性
        })
        this.stresscheck(this.data.stressdata)
        console.log('data', this.data)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})