// pages/index/alarm/alarm.js
const api = require('../../../utils/api')
const util = require('../../../utils/util')
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hasalarm: false,
        addFlag: false,
        alarmList: [],

        startX: 0, //滑动起始位置
        startY: 0,
    },
    //新增闹钟页面跳转
    addAlarm() {
        wx.navigateTo({
            url: '/pages/index/alarm/setAlarm/setalarm',
        })
    },
    //修改闹钟页面跳转
    setAlarm(e) {
        let index = e.currentTarget.dataset.index
        console.log('index', index)
        let obj = JSON.stringify(this.data.alarmList[index])
        wx.navigateTo({
            url: '/pages/index/alarm/setAlarm/setalarm?obj=' + obj,
        })
    },
    //获取闹钟
    getAlarm() {
        const _this = this
        api.getAlarmClockList(app.globalData.token).then(res => {
            if (res.data.code == 200) {
                console.log('success list====>', res.data.data)
                if (res.data.data.length > 0) {
                    res.data.data.forEach((item, index) => {
                        item.time = item.cron.substring(11, 16)
                        item.wake_time = _this.handleAlarm(item)
                        item.isMove = false
                        if (item.user_id != app.globalData.userId) {
                            item.other_one = true
                        } else {
                            item.other_one = false
                        }
                    })
                    _this.setData({
                        alarmList: res.data.data,
                        hasalarm: true
                    })
                    console.log('alarmlist', _this.data.alarmList)
                    const updateWake = setInterval(() => {
                        _this.updateWakeTime()
                    }, 60000)
                } else {
                    app.globalData.hasalarm = false
                    _this.setData({
                        addFlag: true,
                        hasalarm: false,
                        alarmList: []
                    })

                }
            } else {
                util.myShowToast(res.data.msg)
                console.log('failed error====> ', res.data.msg)
            }
        })
    },
    //删除闹钟
    deleteAlarm(e) {
        const _this = this
        let index = e.currentTarget.dataset.index
        console.log('index', e)
        let data = {
            id: this.data.alarmList[index].id
        }
        api.deleteAlarmClock(data, app.globalData.token).then(res => {
            if (res.data.code == 200) {
                console.log('delete success')
                _this.getAlarm()
            } else {
                util.myShowToast(res.data.msg)
                console.log('failed error====> ', res.data.msg)
            }
        })
    },
    //距生效时间时长计算
    handleAlarm(data) {
        let time = new Date(data.cron.replace(/-/g, '/')).getTime() - new Date().getTime()
        if (time < 0) {
            // this.getAlarm()
            return
        }
        let hour = Math.floor(time / (60 * 60 * 1000))
        let mintue = Math.ceil(time % (60 * 60 * 1000) / (60 * 1000))
        console.log('mintue222', mintue)
        if (mintue == 60) {
            return hour + 1 + '小时' + 0 + '分钟'
        } else {
            return hour + '小时' + mintue + '分钟'
        }

    },
    //生效时长更新
    updateWakeTime() {
        let data = this.data.alarmList
        data.forEach(item => {
            item.wake_time = this.handleAlarm(item)
        })
        this.setData({
            alarmList: data
        })
    },
    touchStart(e) {
        let index = e.currentTarget.dataset.index
        console.log('alarmList', this.data.alarmList)
        if (!this.data.alarmList[index].other_one) {
            this.setData({
                startX: e.changedTouches[0].clientX,
                startY: e.changedTouches[0].clientY,
            })
        }
    },
    touchMove(e) {
        let _index = e.currentTarget.dataset.index
        if (!this.data.alarmList[_index].other_one) {
            let {
                startX,
                startY,
                alarmList
            } = this.data
            let {
                clientX,
                clientY
            } = e.changedTouches[0]
            let angle = this.angle({
                X: startX,
                Y: startY
            }, {
                X: clientX,
                Y: clientY
            })
            console.log('alarmList000', alarmList)
            // console.log('angle,index', angle, _index)
            if (Math.abs(angle) > 30) {
                return
            }
            alarmList.forEach((item, index) => {
                item.isMove = false
                if (index == _index && clientX < startX) { //左滑
                    item.isMove = true
                } else { //右滑
                    item.isMove = false
                }
            })
            console.log('alarmList1111', alarmList)
            this.setData({
                alarmList: alarmList
            })
        }

    },
    angle(start, end) {
        let _X = end.X - start.X,
            _Y = end.Y - start.Y
        return 360 * Math.atan(_Y / _X) / 2 * Math.PI
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getAlarm()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})