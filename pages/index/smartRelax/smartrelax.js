// pages/index/smartRelax/smartrelax.js
const utils = require("../../../utils/util")
const app = getApp()
const bluetoothUtils = require("../../../utils/bluetoothUtils")
const network = require('../../../utils/network')
const api = require('../../../utils/api')
let timer = null
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ec_5: {
            lazyLoad: true
        },
        explain1: '三段式10分钟全身放松',
        explain2: '放松过程中操作遥控器任意按钮都会打断智能放松',
        time: 0,
        level: 1,
        functionFlag: false,
        title1: '',
        text1: '',
        text12: '',
        text13: '',
        title2: '',
        text2: '',
        text22: '',
        text23: '',
        text24: '',
        title3: '',
        text31: '',
        text32: '',
        text33: '',
        text34: '',
        text35: '',
        text36: '',
        fixedExplainFlag: false,
        fixStartFlag: false,
        img123Src: '',
        levelTitle: '',
        heartRate: '0',
        breathRate: '0',
        x1: [17, 19, 18, 16, 22, '', '', '', ''],
        x2: [60, 65, 60, 65, 60],
        musicClassName: '虫鸣',
        musicName: '02.夏虫',
        musicTimeNow: '00:00',
        musicTimeLong: '09:30',
        noData: true,
        count: 0, // 设置 计数器 初始为0
        countTimer: null, // 设置 定时器 初始为null
        showProgress: false,
        Title1: '蝗虫式说明',
        Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
        Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
        Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
        Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
        Title2: '眼镜蛇式说明',
        Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
        Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
        Title3: '弓式说明',
        Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
        Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
        Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
        Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
        deviceID: null, //设备ID
        bluetoothNumber: '',
        wifiboxNumber: '',
        familyId: '',
        manyGifFlag: true,
        gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif',
        timer: '',
        gifImg2: '',
        reportSuccessFlag: false,
        bluetoothInfo: null, //记录蓝牙的基本信息
        reconnectionTimes: 0,
        ifConnect: true,
        completedFlag: false,
        timeInterval: null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // this.setData({
        //     bluetoothNumber: options.bluetoothname,
        //     wifiboxNumber: options.wifiboxNumber,
        //     familyId: options.familyId
        // })
        app.globalData.playmusic = wx.getBackgroundAudioManager()
        this.drawProgressbg();
        let _this = this
        this.timer = _this.timer2(1)
    },

    timer2(a) {
        let _this = this
        setTimeout(() => {
            if (a == 0) {
                _this.setData({
                    gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif'
                })
                if (_this.data.manyGifFlag == true) {
                    _this.timer2(1)
                }
            }
            if (a == 1) {
                _this.setData({
                    gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxCobra.gif'
                })
                if (_this.data.manyGifFlag == true) {
                    _this.timer2(2)
                }
            }
            if (a == 2) {
                _this.setData({
                    gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxBowType.gif'
                })
                if (_this.data.manyGifFlag == true) {
                    _this.timer2(0)
                }
            }
        }, 10000);
    },

    time(time1) {
        let _this = this
        timer = setTimeout(() => {
            var myDate = new Date();
            let time = _this.timedifference(time1, myDate.getTime())
            _this.setData({
                time: time
            })
            wx.setStorageSync('sleepTime', time)
            if (this.data.functionFlag == false || time > 600) {
                _this.setData({
                    level: 1,
                    time: 0,
                    explain1: '三段式10分钟全身放松',
                    explain2: '放松过程中操作遥控器任意按钮都会打断智能放松',
                    functionFlag: false,
                    Title1: '蝗虫式说明',
                    Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
                    Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
                    Title2: '眼镜蛇式说明',
                    Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
                    Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
                    Title3: '弓式说明',
                    Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
                    Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
                    Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
                    Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
                    gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif',
                    manyGifFlag: true,
                    showProgress: false
                })
                // if (_this.data.manyGifFlag == true) {
                //     _this.timer2(1)
                // }
                _this.drawProgressCircle(0);
            } else if (time > 0 && time <= 120 && _this.data.explain2 != '蝗虫体式瑜伽体验，放松背部肌肉') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 蝗虫式',
                    explain2: '蝗虫体式瑜伽体验，放松背部肌肉',
                    Title1: '蝗虫式说明',
                    Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
                    Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 120 && time <= 195 && _this.data.explain2 != '眼镜蛇体式瑜伽放松，缓和肩背紧张') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 眼镜蛇式',
                    explain2: '眼镜蛇体式瑜伽放松，缓和肩背紧张',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '眼镜蛇式说明',
                    Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
                    Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxCobra.gif'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 195 && time <= 300 && _this.data.explain2 != '弓体式瑜伽恢复，缓解身心疲劳') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 弓式',
                    explain2: '弓体式瑜伽恢复，缓解身心疲劳',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '弓式说明',
                    Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
                    Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
                    Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
                    Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxBowType.gif'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 300 && time <= 330 && _this.data.explain2 != '40Hz振动按摩，放松减压') {
                _this.setData({
                    level: 2,
                    explain1: '按摩放松中',
                    explain2: '40Hz振动按摩，放松减压',
                    Title1: '40Hz振动按摩',
                    Text11: '40Hz振动按摩放松身心，缓解抑郁，改善睡眠质量',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 330 && time <= 390 && _this.data.explain2 != '44Hz振动按摩，减轻焦虑') {
                _this.setData({
                    level: 2,
                    explain1: '按摩放松中',
                    explain2: '44Hz振动按摩，减轻焦虑',
                    Title1: '44Hz振动按摩',
                    Text11: '44Hz的振动按摩使肌肉放松。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 390 && time <= 510 && _this.data.explain2 != '促进呼吸，52Hz振动按摩，缓和腰部紧张') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '促进呼吸，52Hz振动按摩，缓和腰部紧张',
                    Title1: '头部抬起',
                    Text11: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat.gif'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 510 && time <= 570 && _this.data.explain2 != '促进呼吸，44Hz振动按摩，减轻焦虑') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '促进呼吸，44Hz振动按摩，减轻焦虑',
                    Title1: '头部抬起',
                    Text11: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat.gif'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else if (time > 570 && time <= 600 && _this.data.explain2 != '舒缓身体，放空心静') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '舒缓身体，放空心静',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            } else {
                _this.time(time1)
                _this.drawProgressCircle(time / 600);
            }
        }, 4000);
    },
    //计算两个时间之间的时间差
    timedifference: function (faultDate, completeTime) {
        var stime = Date.parse(new Date(faultDate)); //获得开始时间的毫秒数
        var etime = Date.parse(new Date(completeTime)); //获得结束时间的毫秒数
        var usedTime = etime - stime; //两个时间戳相差的毫秒数
        var days = Math.floor(usedTime / (24 * 3600 * 1000));
        //计算出小时数
        var leave1 = usedTime % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
        var hours = Math.floor(leave1 / (3600 * 1000)); //将剩余的毫秒数转化成小时数
        //计算相差分钟数
        var leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数
        var minutes = Math.floor(leave2 / (60 * 1000)); //将剩余的毫秒数转化成分钟
        //计算相差秒数
        var leave3 = leave2 % (60 * 1000); //计算分钟数后剩余的毫秒数
        var seconds = Math.floor(leave3 / 1000); //将剩余的毫秒数转化成秒数
        var dayStr = days == 0 ? "" : days + "d:";
        var hoursStr = hours == 0 ? "" : hours + "h:";
        var minutesStr = minutes == 0 ? "" : minutes;
        var time = minutesStr * 60 + seconds;
        // console.log(time)
        if (time > 600) {
            console.log('time11111========>', time)
            this.bingLongTap()
        }
        return time;
    },

    setReport: async function (faultDate) {
        var time = new Date(faultDate)
        var start = new Date(faultDate + 180 * 1000)
        var end = new Date(faultDate + 480 * 1000)
        let startTime = this.initTime(time)
        let timeStart = this.initTime(start)
        let timeEnd = this.initTime(end)
        let data = {
            openid: app.globalData.openid,
            deviceId: this.data.wifiboxNumber,
            startTime: startTime,
            timeStart: timeStart,
            timeEnd: timeEnd,
            familyId: this.data.familyId
        }
        let config = {
            url: app.globalData.websize + "/experience/api/v1/relaxReport",
            method: "POST",
            data: data
        }
        let body = await network.request(config);
        this.setData({
            reportSuccessFlag: true
        })
    },

    initTime(faultDate) {
        let time = new Date(faultDate)
        let month = ''
        if (time.getMonth() < 9) {
            month = '0' + (time.getMonth() - 1 + 2)
        } else {
            month = (time.getMonth() - 1 + 2)
        }

        let day = ''
        if (time.getDate() < 10) {
            day = '0' + time.getDate()
        } else {
            day = time.getDate()
        }

        let hour = ''
        if (time.getHours() < 10) {
            hour = '0' + time.getHours()
        } else {
            hour = time.getHours()
        }

        let min = ''
        if (time.getMinutes() < 10) {
            min = '0' + time.getMinutes()
        } else {
            min = time.getMinutes()
        }

        let second = ''
        if (time.getSeconds() < 10) {
            second = '0' + time.getSeconds()
        } else {
            second = time.getSeconds()
        }

        let initTime = time.getFullYear() + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + second
        return initTime
    },

    start: function () {
        let _this = this
        if (app.globalData.bluetooth_connected == true) {
            this.setData({
                functionFlag: true,
                img123Src: '/images/onebuttonsleep/1.png',
                levelTitle: '瑜伽放松',
                fixStartFlag: true,
            })
            _this.bedToStop()
            setTimeout(() => {
                _this.setData({
                    img123Src: '/images/onebuttonsleep/2.png',
                    levelTitle: '按摩放松',
                })
                setTimeout(() => {
                    _this.setData({
                        img123Src: '/images/onebuttonsleep/3.png',
                        levelTitle: '全身放松',
                    })
                    setTimeout(() => {
                        app.globalData.playmusic.title = '智能放松瑜伽'
                        app.globalData.playmusic.src = 'https://download.app.smartbed.ink/inland/music/stress_music/relax_yoga.mp3'
                        _this.setData({
                            img123Src: '/images/onebuttonsleep/1.png',
                            levelTitle: '瑜伽放松',
                            fixStartFlag: false,
                            time: 1,
                            level: 1,
                            explain1: '瑜伽放松 蝗虫式',
                            explain2: '蝗虫体式瑜伽体验，放松背部肌肉',
                            Title1: '蝗虫式说明',
                            Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                            Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
                            Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                            Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
                            Title2: '',
                            Text21: '',
                            Text22: '',
                            Title3: '',
                            Text31: '',
                            Text32: '',
                            Text33: '',
                            Text34: '',
                            showProgress: true,
                            gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif',
                            manyGifFlag: false,
                            // completedFlag: false
                        })
                        var myDate = new Date();
                        this.initData()
                        wx.setStorageSync('smartRelax', myDate.getTime())
                        _this.time(myDate.getTime())
                        _this.startOneButtonRelax()
                    }, 1000);
                }, 1000);
            }, 1000);
        } else {
            console.log('蓝牙已断开')
            // utils.myShowLoading('蓝牙重连中')
            // bluetoothUtils.oneTouchConnectBluetooth(app.globalData.deviceId)
            // setTimeout(() => {
            //     if (app.globalData.bluetooth_connected == true) {
            //         utils.myShowToast('设备已连接')
            //     } else {
            //         utils.myShowToast('连接失败，请重新扫码')
            //     }
            // }, 5000)
        }

    },

    bingLongTap: function () {
        let _this = this
        console.log('test11111========>')
        this.setData({
            level: 1,
            time: 0,
            explain1: '三段式10分钟全身放松',
            explain2: '放松过程中操作遥控器任意按钮都会打断智能放松',
            functionFlag: false,
            title1: '',
            text1: '',
            text12: '',
            text13: '',
            title2: '',
            text2: '',
            text22: '',
            text23: '',
            title3: '',
            text31: '',
            text32: '',
            text33: '',
            text34: '',
            text35: '',
            text36: '',
            Title1: '蝗虫式说明',
            Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
            Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
            Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
            Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
            Title2: '眼镜蛇式说明',
            Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
            Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
            Title3: '弓式说明',
            Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
            Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
            Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
            Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
            showProgress: false,
            gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif',
            manyGifFlag: true,
            completedFlag: true
        })
        app.globalData.playmusic = wx.getBackgroundAudioManager()
        app.globalData.playmusic.stop()
        clearTimeout(timer)
        // _this.timer2(0)
        this.initData()
        _this.drawProgressCircle(0);
        this.bedToStop()
        setTimeout(() => {
            _this.bedToFlat()
        }, 1500);
        wx.vibrateLong();
    },

    fixedExplain: function () {
        this.setData({
            fixedExplainFlag: true
        })
        if (this.data.time > 600) {
            this.setData({
                title1: '',
                text1: '',
                text12: '',
                text13: '',
                title2: '',
                text2: '',
                text22: '',
                text23: '',
                text24: '',
                title3: '',
                text31: '',
                text32: '',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        } else if (this.data.time == 0) {
            this.setData({
                title1: '了解智能放松',
                text1: '    智能放松通过智能床拉伸功能进行瑜伽练习，放松肌肉，延展肢体[4][5][6]；同时配合特定频率的振动按摩，减轻焦虑，放松减压[1][2][3]，达到放松身心的效果。',
                text12: '',
                text13: '',
                title2: '智能放松流程',
                text2: '保持俯卧姿势（趴于智能床上，头部置于床头，双脚置于床位），开启智能放松，您将进行10分钟左右的放松体验，放松肌肉，放松身心。',
                text22: '一阶段瑜伽放松，延展肢体。趴在智能床上跟随语音与动态图引导，依次进行蝗虫式、眼镜蛇式和弓式瑜伽体式练习，放松肌肉，延展肢体。这个过程中智能床将会进行一定角度的抬升辅助您的瑜伽练习。',
                text23: '二阶段按摩放松，减轻焦虑。回到仰卧姿势（正躺在智能床上），智能床开启特定频率的振动按摩，放松身心，改善睡眠质量。',
                text24: '三阶段全身舒缓，深度放松。保持仰卧姿势，智能床开启头部抬起与振动按摩，促进呼吸，缓解焦虑，深度放松身心。',
                title3: '参考资料',
                text31: '[1] Braun Janzen, Thenille; Paneduro, Denise; Picard, Larry et al. A parallel randomized controlled trial examining the effects of rhythmic sensory stimulation on fibromyalgia symptoms[J]. PLOS ONE, 2019,14(3). ',
                text32: ' [2] Boyd-Brewer C, McCaffrey R.Vibroacoustic Sound Therapy Improves Pain Management and More[J]. Holistic Nursing Practice,2004,18(3),111–118.',
                text33: ' [3] Ala-Ruona, E., Punkanen, M., & Campbell, E. Vibroacoustic Therapy:Conception,Development, and Future Directions[J].Musiikkiterapia, 2015,30(1-2),48-71.',
                text34: '[4] 李若果,熊玉玲.“瑜伽放松休息术”对运动性疲劳的恢复的影响研究[J].体育世界(学术版),2019(01):157-158.DOI:10.16730/j.cnki.61-1019/g8.2019.01.102.',
                text35: ' [5] 刘怿,许荣梅.瑜伽联合意境训练对更年期女性焦虑情绪及睡眠质量的影响[J].中华物理医学与康复杂志,2019(09):698-699.',
                text36: '[6] 孟建平.瑜伽锻炼对老年人情绪及睡眠质量的影响[J].中国老年学杂志,2013,33(18):4568-4569.',
            })
        } else if (this.data.time > 0 && this.data.time <= 120) {
            this.setData({
                title1: '蝗虫式',
                text1: '蝗虫式(Salabhasana)从俯卧位进入。双腿伸直并抬起；手臂向后伸直，掌心向下，抬起；抬起头，视线直直地指向前方[1] 。',
                text12: '这是一种背部弯曲或脊柱伸展，利用上背部和中背部的力量，从起始位置尽可能高地抬起双腿的重量，同时面朝下放在地板上。它可以提高柔韧性和协调性，锻炼背部肌肉，增加力量和耐力[2]。',
                text13: '',
                title2: '瑜伽',
                text2: '瑜伽放松有利于放松肌肉，恢复身体机能，提高运动性疲劳恢复效果[3]。',
                text22: '放松肌肉，改善情绪，提高睡眠质量：结合瑜伽体式,使人在轻松愉悦的瑜伽氛围中拉伸肌肉、伸展整个肢体，同时在此过程中排除所有杂念、使紧张的神经得以舒缓放松，身心处于放松状态，从而达到恢复疲劳的作用，进一步缓解不良情绪及提高睡眠质量[4]。',
                text23: '瑜伽运动能有效地改善睡眠质量，调节抑郁、紧张、焦虑等不良情绪，并对身心健康起到积极的作用[5]。',
                text24: '',
                title3: '理论依据：',
                text31: '[1] Mehta 1990 年，p. 92.   ',
                text32: '[2] Stiles，穆昆达 (2000)。结构瑜伽疗法：适应个人。红轮。国际标准书号 1-57863-177-7.',
                text33: '[3] 李若果,熊玉玲.“瑜伽放松休息术”对运动性疲劳的恢复的影响研究[J].体育世界(学术版),2019(01):157-158.DOI:10.16730/j.cnki.61-1019/g8.2019.01.102.',
                text34: '[4] 刘怿,许荣梅.瑜伽联合意境训练对更年期女性焦虑情绪及睡眠质量的影响[J].中华物理医学与康复杂志,2019(09):698-699.',
                text35: '[5] 孟建平.瑜伽锻炼对老年人情绪及睡眠质量的影响[J].中国老年学杂志,2013,33(18):4568-4569.',
                text36: '',
            })
        } else if (this.data.time > 120 && this.data.time <= 195) {
            this.setData({
                title1: '眼镜蛇式',
                text1: '眼镜蛇式手掌放在肩膀下方，向下推直到臀部略微抬起。脚背着床，双腿伸直；凝视向前，做出准备姿势。对于完整的姿势，背部拱起直到手臂伸直，视线直接向上或稍微向后[1] 。',
                text12: '通过在胸部有意识地打开并在肩膀上伸展，可以对抗疲劳并缓解腰痛，增强精力和身体。如果您遇到背痛、肩膀紧绷或上半身酸痛，可以提供缓解[2]。',
                text13: '眼镜蛇式可增加脊柱的活动性，增强脊柱支撑肌肉，并有助于缓解背部疼痛。它打开胸部和身体的前部。如果您整天坐着，这可能特别有用。久坐会导致胸部肌肉紧绷和背部肌肉拉伸、减弱。眼镜蛇可以抵消这种弯腰驼背的姿势[3]。',
                title2: '瑜伽',
                text2: '瑜伽放松有利于放松肌肉，恢复身体机能，提高运动性疲劳恢复效果[4]。',
                text22: '放松肌肉，改善情绪，提高睡眠质量：结合瑜伽体式,使人在轻松愉悦的瑜伽氛围中拉伸肌肉、伸展整个肢体，同时在此过程中排除所有杂念、使紧张的神经得以舒缓放松，身心处于放松状态，从而达到恢复疲劳的作用，进一步缓解不良情绪及提高睡眠质量[5]。',
                text23: '瑜伽运动能有效地改善睡眠质量，调节抑郁、紧张、焦虑等不良情绪，并对身心健康起到积极的作用[6]。',
                text24: '',
                title3: '参考资料：',
                text31: '[1] 艾扬格，BKS (1979) [1966]。瑜伽之光。肖肯。第 107-108、396-397 页。国际标准书号 0-8052-1031-8.',
                text32: '[2] https://www.yogajournal.com/poses/cobra-pose-2/',
                text33: '[3] https://www.verywellfit.com/cobra-pose-bhujangasana-3567067',
                text34: '[4] 李若果,熊玉玲.“瑜伽放松休息术”对运动性疲劳的恢复的影响研究[J].体育世界(学术版),2019(01):157-158.DOI:10.16730/j.cnki.61-1019/g8.2019.01.102.',
                text35: '[5] 刘怿,许荣梅.瑜伽联合意境训练对更年期女性焦虑情绪及睡眠质量的影响[J].中华物理医学与康复杂志,2019(09):698-699.',
                text36: '[6] 孟建平.瑜伽锻炼对老年人情绪及睡眠质量的影响[J].中国老年学杂志,2013,33(18):4568-4569.',
            })
        } else if (this.data.time > 195 && this.data.time <= 300) {
            this.setData({
                title1: '弓式',
                text1: '弓式（Dhanurasana）躺在垫子上，将所有脚趾接触地面，然后弯曲膝盖，保持脚趾活动。用手抓住脚踝的外缘，用力弯曲双脚。吸气时，将肋骨笼和肩膀抬向耳朵。呼气时，拉长尾骨，双腿踢回手中，同时牢牢抓住。从这里，抬起头部，向前凝视。通过大腿向下按压以抬起胸部。初学者很难将大腿抬离地板。可以通过躺在卷起的毯子上支撑大腿来给腿一点向上的提升。[1] 。',
                text12: '弓姿势是充满活力的，刺激肾上腺，这可以帮助你对抗疲劳。它还会增加流向消化系统的血液。它可能有助于建立信心和赋权。弓姿势还可以改善姿势并抵消长时间坐着的影响，例如懒散和脊柱后凸（脊柱的异常弯曲）。它可能有助于缓解背痛。它伸展你的腹部，胸部，肩膀，臀部前部（髋部屈肌）和大腿前部（股四头肌）。弓姿势可以增强您的背部肌肉，大腿后部和臀部（臀部）。[1]。',
                text13: '',
                title2: '瑜伽',
                text2: '瑜伽放松有利于放松肌肉，恢复身体机能，提高运动性疲劳恢复效果[2]。',
                text22: '放松肌肉，改善情绪，提高睡眠质量：结合瑜伽体式,使人在轻松愉悦的瑜伽氛围中拉伸肌肉、伸展整个肢体，同时在此过程中排除所有杂念、使紧张的神经得以舒缓放松，身心处于放松状态，从而达到恢复疲劳的作用，进一步缓解不良情绪及提高睡眠质量[3]。',
                text23: '瑜伽运动能有效地改善睡眠质量，调节抑郁、紧张、焦虑等不良情绪，并对身心健康起到积极的作用[4]。',
                text24: '',
                title3: '参考资料：',
                text31: '[1] https://www.yogajournal.com/poses/bow-pose/',
                text32: '[2] 李若果,熊玉玲.“瑜伽放松休息术”对运动性疲劳的恢复的影响研究[J].体育世界(学术版),2019(01):157-158.DOI:10.16730/j.cnki.61-1019/g8.2019.01.102.',
                text33: '[3] 刘怿,许荣梅.瑜伽联合意境训练对更年期女性焦虑情绪及睡眠质量的影响[J].中华物理医学与康复杂志,2019(09):698-699.',
                text34: '[4] 孟建平.瑜伽锻炼对老年人情绪及睡眠质量的影响[J].中国老年学杂志,2013,33(18):4568-4569.',
                text35: '',
                text36: '',
            })
        } else if (this.data.time > 300 && this.data.time <= 330) {
            this.setData({
                title1: '40Hz振动按摩',
                text1: '40Hz振动按摩放松身心，缓解抑郁，改善睡眠质量[1]。',
                text12: '',
                text13: '',
                title2: '理论依据：',
                text2: '[1] Braun Janzen, Thenille; Paneduro, Denise; Picard, Larry et al. A parallel randomized controlled trial examining the effects of rhythmic sensory stimulation on fibromyalgia symptoms[J]. PLOS ONE, 2019,14(3). ',
                text22: '',
                text23: '',
                text24: '',
                title3: '',
                text31: '',
                text32: '',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        } else if (this.data.time > 330 && this.data.time <= 390) {
            this.setData({
                title1: '44Hz振动按摩',
                text1: '44Hz的振动按摩使肌肉放松[1]。',
                text12: '',
                text13: '',
                title2: '理论依据：',
                text2: '[1] Boyd-Brewer C, McCaffrey R.Vibroacoustic Sound Therapy Improves Pain Management and More[J]. Holistic Nursing Practice,2004,18(3),111–118.',
                text22: '',
                text23: '',
                text24: '',
                title3: '',
                text31: '',
                text32: '',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        } else if (this.data.time > 390 && this.data.time <= 510) {
            this.setData({
                title1: '头部抬起',
                text1: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义[1]。',
                text12: '',
                text13: '',
                title2: '52Hz振动按摩',
                text2: '52Hz振动按摩减轻疼痛[2]。',
                text22: '',
                text23: '',
                text24: '',
                title3: '理论依据：',
                text31: '[1] Souza FJFB, Genta PR, de Souza Filho AJ, et al. The influence of head-of-bed elevation in patients with obstructive sleep apnea[J]. Sleep Breath. 2017 ;21(4):815-820. ',
                text32: '[2] Ala-Ruona, E., Punkanen, M., & Campbell, E. Vibroacoustic Therapy:Conception,Development, and Future Directions[J].Musiikkiterapia, 2015,30(1-2),48-71.',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        } else if (this.data.time > 510 && this.data.time <= 570) {
            this.setData({
                title1: '头部抬起',
                text1: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义[1]。',
                text12: '',
                text13: '',
                title2: '44Hz振动按摩',
                text2: '44Hz的振动按摩使肌肉放松[2]。',
                text22: '',
                text23: '',
                text24: '',
                title3: '理论依据：',
                text31: '[1] Souza FJFB, Genta PR, de Souza Filho AJ, et al. The influence of head-of-bed elevation in patients with obstructive sleep apnea[J]. Sleep Breath. 2017 ;21(4):815-820. ',
                text32: '[2] Boyd-Brewer C, McCaffrey R.Vibroacoustic Sound Therapy Improves Pain Management and More[J]. Holistic Nursing Practice,2004,18(3),111–118.',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        } else if (this.data.time > 570 && this.data.time <= 600) {
            this.setData({
                title1: '',
                text1: '',
                text12: '',
                text13: '',
                title2: '',
                text2: '',
                text22: '',
                text23: '',
                text24: '',
                title3: '',
                text31: '',
                text32: '',
                text33: '',
                text34: '',
                text35: '',
                text36: '',
            })
        }
    },

    close: function () {
        this.setData({
            fixedExplainFlag: false
        })
    },
    //重新进入时重绘
    reDraw() {
        let _this = this
        let temp = wx.getStorageSync('smartRelax')
        if (temp != null || temp != undefined) {
            temp = parseInt(temp)
            var myDate = new Date();
            let time = _this.timedifference(new Date(temp).getTime(), myDate.getTime())
            _this.setData({
                time: time,
                showProgress: true,
                functionFlag: true
            })
            if (this.data.functionFlag == false || time > 600) {
                _this.setData({
                    level: 1,
                    time: 0,
                    explain1: '三段式10分钟全身放松',
                    explain2: '放松过程中操作遥控器任意按钮都会打断智能放松',
                    functionFlag: false,
                    Title1: '蝗虫式说明',
                    Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
                    Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
                    Title2: '眼镜蛇式说明',
                    Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
                    Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
                    Title3: '弓式说明',
                    Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
                    Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
                    Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
                    Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
                    gifImg: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif',
                    manyGifFlag: true,
                    showProgress: false
                })
                // if (_this.data.manyGifFlag == true) {
                //     _this.timer2(1)
                // }
                _this.drawProgressCircle(0);
            } else if (time > 0 && time <= 120 && _this.data.explain2 != '蝗虫体式瑜伽体验，放松背部肌肉') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 蝗虫式',
                    explain2: '蝗虫体式瑜伽体验，放松背部肌肉',
                    Title1: '蝗虫式说明',
                    Text11: '   俯卧，双臂向后伸展，掌心相对，如果向后伸展较为困难，可将双臂贴着床垫向前伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text12: '呼气 ， 身体随着智能床角度改变，放松身体，保持呼吸自然。',
                    Text13: ' 重复体式，双臂向前或者向后伸展，下巴贴着床垫，吸气，收缩臀部，双腿并拢，随着智能床慢慢抬起上半身和双腿，注意腹部不要离开床垫，保持呼吸自然。',
                    Text14: '身体随智能床自然放平，肌肉放松。这个体式可以舒展腰腹、上臂和大腿肌肉，增强脊椎的弹性，还可以帮助消化，减缓肠胃不适。',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLocustType.gif'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 120 && time <= 195 && _this.data.explain2 != '眼镜蛇体式瑜伽放松，缓和肩背紧张') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 眼镜蛇式',
                    explain2: '眼镜蛇体式瑜伽放松，缓和肩背紧张',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '眼镜蛇式说明',
                    Text21: '   保持俯卧姿势，双脚并拢，膝盖绷直，脚趾指向后，双手放在胸部的两侧，手掌按住床垫。吸气，上半身随着智能床抬起，注意脖子不要后仰，脊柱向前向上延展，胸腔打开，将身体重量放在双腿和双掌上，保持呼吸自然。',
                    Text22: '身体再次随智能床放平，肌肉放松。这个体式，能让胸部得到完全扩展，脊柱得以充分的锻炼，对腹部和臀部的塑形有很好的作用。有胃溃疡和严重脊椎病的人群，拉伸时需注意幅度，无需太过用力。',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxCobra.gif'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 195 && time <= 300 && _this.data.explain2 != '弓体式瑜伽恢复，缓解身心疲劳') {
                _this.setData({
                    level: 1,
                    explain1: '瑜伽放松 弓式',
                    explain2: '弓体式瑜伽恢复，缓解身心疲劳',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '弓式说明',
                    Text31: '   缓慢而深长的吸气，屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝，如果两手难以碰到脚踝，可抓住脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，呼吸。 ',
                    Text32: '松开手，腿部随着智能床恢复平直延展，身体放松。',
                    Text33: '再次屈膝，双脚向上弯曲，脚跟接近臀部，膝盖打开，与髋部同宽，同时双臂向后伸展，两手分别抓住同侧脚踝或者脚趾。随着智能床的升起，双手发力抬高双腿，头部微抬，胸腔打开，同时保持呼吸自然，注意放松腹部，不要屏气。',
                    Text34: '松开手，身体随着智能床放平。这个体式有助于锻炼大腿和臀部肌肉，消除腹部和背部的赘肉，美化身体曲线。还能改善脾胃功能，增强胰脏活力，对于肾上腺、甲状旁腺、脑下垂体及性腺都有很好的影响。',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxBowType.gif'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 300 && time <= 330 && _this.data.explain2 != '40Hz振动按摩，放松减压') {
                _this.setData({
                    level: 2,
                    explain1: '按摩放松中',
                    explain2: '40Hz振动按摩，放松减压',
                    Title1: '40Hz振动按摩',
                    Text11: '40Hz振动按摩放松身心，缓解抑郁，改善睡眠质量',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 330 && time <= 390 && _this.data.explain2 != '44Hz振动按摩，减轻焦虑') {
                _this.setData({
                    level: 2,
                    explain1: '按摩放松中',
                    explain2: '44Hz振动按摩，减轻焦虑',
                    Title1: '44Hz振动按摩',
                    Text11: '44Hz的振动按摩使肌肉放松。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 390 && time <= 510 && _this.data.explain2 != '促进呼吸，52Hz振动按摩，缓和腰部紧张') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '促进呼吸，52Hz振动按摩，缓和腰部紧张',
                    Title1: '头部抬起',
                    Text11: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat.gif'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 510 && time <= 570 && _this.data.explain2 != '促进呼吸，44Hz振动按摩，减轻焦虑') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '促进呼吸，44Hz振动按摩，减轻焦虑',
                    Title1: '头部抬起',
                    Text11: '抬高床头对降低呼吸暂停低通气指数，提高血氧饱和度具有显著意义。',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat.gif'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else if (time > 570 && time <= 600 && _this.data.explain2 != '舒缓身体，放空心静') {
                _this.setData({
                    level: 3,
                    explain1: '全身放松中',
                    explain2: '舒缓身体，放空心静',
                    Title1: '',
                    Text11: '',
                    Text12: '',
                    Text13: '',
                    Text14: '',
                    Title2: '',
                    Text21: '',
                    Text22: '',
                    Title3: '',
                    Text31: '',
                    Text32: '',
                    Text33: '',
                    Text34: '',
                    gifImg2: 'https://ks-experience-system.oss-cn-hangzhou.aliyuncs.com/softide/version_2/experience/relaxLieFlat2.png'
                })
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            } else {
                _this.time(new Date(temp).getTime())
                _this.drawProgressCircle(time / 600);
            }
        }
    },
    //缓存数据初始化
    initData() {
        app.globalData.BRDATA = null
        app.globalData.HRDATA = null
        app.globalData.setBR = null
        app.globalData.setHR = null
        wx.setStorageSync('smartRelax', null)
        wx.setStorageSync('oneManSleep', null)
        wx.setStorageSync('remote', null)
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.reDraw()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
        clearTimeout(timer)
        // let _this = this
        // if (this.data.functionFlag == true) {
        //     this.bedToStop()
        //     setTimeout(() => {
        //         _this.bedToFlat()
        //     }, 100);
        //     this.setData({
        //         functionFlag: false,
        //     })
        // }
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        clearTimeout(timer)
        // let _this = this
        // this.bedToStop()
        // // setTimeout(() => {
        // //     _this.bedToFlat()
        // // }, 200);
        // this.setData({
        //     functionFlag: false,
        // })

        // // setTimeout(() => {
        // //     let closeCon = utils.closeBLEConnection(_this.data.deviceID);
        // // }, 300);
        // app.globalData.playmusic.stop()
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 绘制灰色背景
     */
    drawProgressbg: function () {
        // 使用 wx.createContext 获取绘图上下文 context
        var ctx = null;
        wx.createSelectorQuery()
            .select("#canvasProgressbg")
            .context(function (res) {
                // console.log("节点实例：", res);
                // 节点对应的 Canvas 实例。
                ctx = res.context;
                ctx.setLineWidth(30); // 设置圆环的宽度
                ctx.setStrokeStyle('#79E5FF'); // 设置圆环的颜色
                ctx.setLineCap('round') // 设置圆环端点的形状
                ctx.beginPath(); //开始一个新的路径
                ctx.arc(150, 151, 135, -Math.PI, 0, false);
                //设置一个原点(110,110)，半径为100的圆的路径到当前路径
                ctx.stroke(); //对当前路径进行描边
                ctx.draw();
            })
            .exec();


    },
    /**
     * 绘制小程序进度
     * @param {*} step 
     */
    drawProgressCircle: function (step) {
        console.log('test2222========>', step, this.data.showProgress, this.data.functionFlag)
        if (this.data.showProgress == true && this.data.functionFlag == true) {
            let ctx = null;
            wx.createSelectorQuery()
                .select("#canvasProgress")
                .context(function (res) {
                    // console.log("节点实例：", res); // 节点对应的 Canvas 实例。
                    if (res) {
                        ctx = res.context;
                        // 设置渐变
                        var gradient = ctx.createLinearGradient(200, 100, 100, 200);
                        ctx.setLineWidth(30);
                        ctx.setStrokeStyle('#1EC6FF'); // 设置圆环的颜色
                        ctx.setLineCap('round')
                        ctx.beginPath();
                        // 参数step 为绘制的圆环周长，从0到2为一周 。 -Math.PI / 2 将起始角设在12点钟位置 ，结束角 通过改变 step 的值确定
                        ctx.arc(150, 151, 135, -Math.PI, step * Math.PI - Math.PI, false);
                        ctx.stroke();
                        ctx.draw()
                    }
                })
                .exec();
        } else {
            if (this.data.completedFlag == true) {
                let ctx = null;
                wx.createSelectorQuery()
                    .select("#canvasProgress")
                    .context(function (res) {
                        // console.log("节点实例：", res); // 节点对应的 Canvas 实例。
                        if (res) {
                            ctx = res.context;
                            // 设置渐变
                            var gradient = ctx.createLinearGradient(200, 100, 100, 200);
                            ctx.setLineWidth(30);
                            ctx.setStrokeStyle('#1EC6FF'); // 设置圆环的颜色
                            ctx.setLineCap('round')
                            ctx.beginPath();
                            // 参数step 为绘制的圆环周长，从0到2为一周 。 -Math.PI / 2 将起始角设在12点钟位置 ，结束角 通过改变 step 的值确定
                            ctx.arc(150, 151, 135, -Math.PI, step * Math.PI - Math.PI, false);
                            ctx.stroke();
                            ctx.draw()
                        }
                    })
                    .exec();
            }
        }
    },

    //开启一键放松
    startOneButtonRelax: async function () {
        let _this = this;
        // let sendmessageRes = await this.sendMsg("E6FE160800000000FD");
        //先打断0.6s后发送指令
        let sendmessageRes0 = await bluetoothUtils.sendMsg("AA03000A00040100000002F8F355");
        await bluetoothUtils.wait(0.6)
        let sendmessageRes1 = await bluetoothUtils.sendMsg("AA03000A00040100000002F8F355");
        await bluetoothUtils.wait(0.6)
        let sendmessageRes = await bluetoothUtils.sendMsg("AA03000A000401000000807AF355");
        utils.insertRemoteKey(app.globalData.deviceId, 'one_key_relax', "AA03000A000401000000807AF355")
        // console.log("开启一键放松", sendmessageRes);
        if (!sendmessageRes) {
            bluetoothUtils.myShowToast("蓝牙连接中断");
        }
        // let data = {
        //     device_id: app.globalData.deviceId,
        //     scene_key: 'one_key_relax'
        // }
        // api.setBedControl(data, app.globalData.token).then(res => {
        //     console.log('control', res)
        // })
    },
    // 一键放平
    bedToFlat: async function () {
        let _this = this;
        // let sendmessageRes = await this.sendMsg("E6FE160000000800FD");
        let sendmessageRes = await bluetoothUtils.sendMsg("AA03000A00040100000008F2F355");
        console.log("一键放平:", sendmessageRes);
        if (!sendmessageRes) {
            utils.myShowToast("蓝牙连接中断，请重试");
        }
    },

    //打断
    bedToStop: async function () {
        let _this = this;
        // let sendmessageRes = await this.sendMsg("E6FE160000000800FD");
        let sendmessageRes = await bluetoothUtils.sendMsg("AA03000A00040100000002F8F355");
        console.log("一键打断:", sendmessageRes);
        if (!sendmessageRes) {
            utils.myShowToast("蓝牙连接中断，请重试");
        }
    },
})